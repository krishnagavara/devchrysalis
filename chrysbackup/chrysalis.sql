-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2017 at 07:59 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chrysalis`
--

-- --------------------------------------------------------

--
-- Table structure for table `cc_address_master`
--

CREATE TABLE `cc_address_master` (
  `address_id` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `location_name` varchar(200) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(127) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT '0',
  `phone` varchar(255) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `lat` varchar(127) DEFAULT NULL,
  `lng` varchar(127) DEFAULT NULL,
  `address_type` enum('basic','billing','request_a_bag','shipping','costume','selling') NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_address_master`
--

INSERT INTO `cc_address_master` (`address_id`, `fname`, `lname`, `location_name`, `address1`, `address2`, `city`, `state`, `country`, `zip_code`, `phone`, `fax`, `lat`, `lng`, `address_type`, `user_id`, `created_on`) VALUES
(261, 'jithender test1', NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, NULL, 'costume', 86, '2017-06-06 07:43:01'),
(262, 'jithender test1', NULL, '', '104', 'Princes Highway', 'Bolwarra', 'VIC', NULL, '3305', '', NULL, NULL, NULL, 'costume', 86, '2017-06-06 07:44:16'),
(263, 'jithender test1', NULL, '', '1001', 'Massachusetts Avenue', 'Lexington', 'MA', NULL, '02420', '', NULL, NULL, NULL, 'costume', 86, '2017-06-06 07:45:41'),
(264, 'jithendertest2', NULL, '', '1000', 'Atlantic Avenue', '', 'NY', NULL, '11238', '', NULL, NULL, NULL, 'costume', 87, '2017-06-06 07:47:25'),
(265, 'jithendertest2', NULL, '', '1001', 'Massachusetts Avenue', 'Lexington', 'MA', NULL, '02420', '', NULL, NULL, NULL, 'costume', 87, '2017-06-06 07:48:39'),
(266, 'jithendertest2', 'kumar', '', '1099', 'Massachusetts Avenue', 'Lexington', 'MA', NULL, '02420', NULL, NULL, NULL, NULL, 'selling', 87, '2017-06-06 07:53:10'),
(267, 'jithender', 'kumar', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', 'United States', '39201', NULL, NULL, NULL, NULL, 'shipping', 88, '2017-06-06 08:46:28'),
(268, 'jithender', 'kumar', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', 'United States', '39201', NULL, NULL, NULL, NULL, 'billing', 88, '2017-06-06 08:46:28'),
(269, 'jithender test1', 'kumar', '', '20815', 'Grand River Avenue', 'Detroit', 'MI', NULL, '48219', NULL, NULL, NULL, NULL, 'selling', 86, '2017-06-06 10:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `cc_attributes`
--

CREATE TABLE `cc_attributes` (
  `attribute_id` int(11) NOT NULL,
  `code` varchar(127) NOT NULL,
  `label` varchar(127) NOT NULL,
  `type` varchar(127) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_attributes`
--

INSERT INTO `cc_attributes` (`attribute_id`, `code`, `label`, `type`, `created_at`, `updated_at`) VALUES
(1, 'body-dimensions', 'Body & Dimensions', 'input', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(2, 'cosplay', 'Is your Costume Used for Cosplay?*', 'radio', '2017-03-31 00:00:00', '2017-04-25 05:43:00'),
(3, 'fashion', 'Is your Costume Unique Fashion?*', 'radio', '2017-03-31 00:00:00', '2017-04-25 05:43:23'),
(4, 'activity', 'Is your Costume used for an Activity?*', 'radio', '2017-03-31 00:00:00', '2017-04-25 05:43:12'),
(5, 'make_costume', 'Did you  make your Costume?*', 'radio', '2017-03-31 00:00:00', '2017-04-28 01:36:43'),
(6, 'costume_desc', '\n\nCostume Description *\n', 'texarea', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(7, 'fun_fact', 'Fun Facts', 'textarea', '2017-03-31 00:00:00', '2017-04-22 02:54:18'),
(8, 'faq', 'FAQ', 'texarea', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(9, 'shipping_option', 'Shipping Option', 'select', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(10, 'weight_package_items', 'Weight of the Packaged Item', 'select', '2017-03-31 00:00:00', '2017-04-22 02:56:14'),
(11, 'dimensions', 'Dimensions', 'text', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(12, 'type', 'Type', 'select', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(13, 'service', 'Service', 'select', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(14, 'handling_time', 'Handling Time', 'select', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(15, 'return_policy', 'Return Policy', 'select', '2017-03-31 00:00:00', '2017-03-31 00:00:00'),
(16, 'heightft', 'Height-ft', 'text', '2017-04-11 05:29:49', '2017-04-25 01:40:06'),
(17, 'heightin', 'Height-in', 'text', '2017-04-11 05:30:16', '2017-04-25 01:40:19'),
(18, 'weightlbs', 'Weight-lbs', 'text', '2017-04-11 05:30:49', '2017-04-25 01:40:52'),
(19, 'chestin', 'Chest-in', 'text', '2017-04-11 05:31:09', '2017-04-25 01:39:53'),
(20, 'waistlbs', 'Waist-lbs', 'text', '2017-04-11 05:31:42', '2017-04-25 01:40:40'),
(21, 'fimquality', ' Is the costume fit for Film Quality? ', 'radio', '2017-04-11 08:45:13', '0000-00-00 00:00:00'),
(22, 'lengthin', 'Length-in', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'widthin', 'Width-in', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'height_in', 'Height-in', 'text', '2017-04-20 00:00:00', '2017-04-20 00:00:00'),
(25, 'cosplay_activity', 'Cosplay Activity', 'radio', '2017-04-24 05:42:49', '0000-00-00 00:00:00'),
(26, 'unique_fashion', 'Unique Fashion', 'radio', '2017-04-24 05:43:16', '0000-00-00 00:00:00'),
(28, 'an_activity', 'An Activity ', 'radio', '2017-04-24 05:44:19', '0000-00-00 00:00:00'),
(29, 'costume_time', 'Make a Costume time', 'text', '2017-04-24 09:42:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_attribute_options`
--

CREATE TABLE `cc_attribute_options` (
  `option_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `option_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_attribute_options`
--

INSERT INTO `cc_attribute_options` (`option_id`, `attribute_id`, `option_value`) VALUES
(1, 1, 'height-ft'),
(2, 1, 'height-in'),
(3, 1, 'weight-lbs'),
(4, 1, 'weight-in'),
(5, 1, 'chest-in'),
(6, 1, 'waist-lbs'),
(7, 2, 'Yes'),
(8, 2, 'No'),
(9, 3, 'Yes'),
(10, 3, 'No'),
(11, 4, 'Yes'),
(12, 4, 'No'),
(13, 6, 'description'),
(14, 7, 'description'),
(15, 8, 'description'),
(16, 9, 'Free Shipping'),
(17, 10, '1+ -20lbs'),
(18, 10, '2+ -201lbs'),
(19, 11, 'length-in'),
(20, 11, 'width-in'),
(21, 11, 'height-in'),
(22, 12, 'Postage (for thin envelope)'),
(23, 12, 'Postage (for thick envelope)'),
(24, 13, 'USPS Media Mail(2-8 Business Hours)'),
(25, 13, 'USPS Media Mail(10-18 Business Hours)'),
(26, 14, 'Same Business Day'),
(27, 14, '10 Business Days'),
(28, 15, 'Return Accepted'),
(29, 15, 'Return Not Accepted '),
(30, 5, 'Yes'),
(31, 5, 'No'),
(32, 21, 'Yes'),
(33, 21, 'No'),
(34, 25, 'Anime/Manga'),
(35, 25, 'Sci-Fi'),
(36, 25, 'Cosmic/Superhero'),
(37, 25, 'Video Games'),
(38, 25, 'Furries'),
(39, 25, 'Other'),
(40, 25, 'Film & Tv'),
(41, 25, 'Mecha'),
(42, 26, 'Cyberpunk'),
(43, 26, 'Lolita'),
(44, 26, 'Dystopain'),
(45, 26, 'Mori kei'),
(46, 26, 'Goth'),
(47, 26, 'Fari kei'),
(48, 26, 'Steampunk'),
(49, 26, 'Visual kei'),
(50, 26, 'Streetwear'),
(51, 26, 'Other'),
(52, 28, 'Circus'),
(53, 28, 'Theatre'),
(54, 28, 'Historical Reenactments'),
(55, 28, 'Music Videos'),
(56, 28, 'LARP'),
(57, 28, 'Masquerade'),
(58, 28, 'Medieval/Renaissance Fairs'),
(59, 29, 'make_a_costume_time'),
(60, 17, '6.7'),
(61, 13, 'FIRST CLASS'),
(62, 13, 'FIRST CLASS COMMERCIAL'),
(63, 13, 'FIRST CLASS HFP COMMERCIAL'),
(64, 13, 'PRIORITY'),
(65, 13, 'PRIORITY COMMERCIAL'),
(66, 13, 'PRIORITY HFP COMMERCIAL'),
(67, 13, 'EXPRESS'),
(68, 13, 'EXPRESS COMMERCIAL'),
(69, 13, 'EXPRESS SH'),
(70, 13, 'EXPRESS SH COMMERCIAL'),
(71, 13, 'EXPRESS HFP'),
(72, 13, 'EXPRESS HFP COMMERCIAL'),
(73, 13, 'PARCEL'),
(74, 13, 'MEDIA'),
(75, 13, 'LIBRARY'),
(76, 13, 'ALL'),
(77, 13, 'ONLINE'),
(78, 9, 'USPS Shipping');

-- --------------------------------------------------------

--
-- Table structure for table `cc_blog_categories`
--

CREATE TABLE `cc_blog_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(160) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_blog_categories`
--

INSERT INTO `cc_blog_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'category 1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'category 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'category 3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'category 4', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_blog_posts`
--

CREATE TABLE `cc_blog_posts` (
  `id` int(11) NOT NULL,
  `title` varchar(160) NOT NULL,
  `description` text NOT NULL,
  `posted_by` varchar(160) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `categories` varchar(512) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_blog_posts`
--

INSERT INTO `cc_blog_posts` (`id`, `title`, `description`, `posted_by`, `status`, `categories`, `created_at`, `updated_at`) VALUES
(1, 'Google', 'google me<br><p><br></p>', '0', 1, '1,2,3,4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Facebook', 'google<p><br></p>', '0', 0, '1,2,3,4', '2017-06-03 04:05:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_cart`
--

CREATE TABLE `cc_cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `cookie_id` varchar(255) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `phone_no` varchar(31) NOT NULL,
  `pay_firstname` varchar(31) NOT NULL,
  `pay_lastname` varchar(31) NOT NULL,
  `pay_address_1` varchar(127) NOT NULL,
  `pay_address_2` varchar(127) NOT NULL,
  `pay_city` varchar(127) NOT NULL,
  `pay_zipcode` varchar(31) NOT NULL,
  `pay_state` varchar(100) NOT NULL,
  `pay_country` varchar(127) NOT NULL,
  `payment_method` varchar(127) NOT NULL,
  `payment_code` varchar(127) NOT NULL,
  `shipping_firstname` varchar(31) NOT NULL,
  `shipping_lastname` varchar(31) NOT NULL,
  `shipping_address_1` varchar(127) NOT NULL,
  `shipping_address_2` varchar(127) NOT NULL,
  `shipping_city` varchar(127) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_state` varchar(100) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(10,4) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `coupan_code` varchar(100) NOT NULL,
  `store_credits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_cart`
--

INSERT INTO `cc_cart` (`cart_id`, `user_id`, `cookie_id`, `firstname`, `lastname`, `email`, `phone_no`, `pay_firstname`, `pay_lastname`, `pay_address_1`, `pay_address_2`, `pay_city`, `pay_zipcode`, `pay_state`, `pay_country`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_state`, `shipping_country`, `shipping_method`, `shipping_code`, `comment`, `total`, `affiliate_id`, `commission`, `cc_id`, `coupan_code`, `store_credits`, `created_at`, `modified_at`) VALUES
(135, 88, 'WBztpwLWBI4lXi2tTD3ZYzWgesvTU4KvpS7DX2NYEAaj4WSWRWhTYud3U42ykJe8', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '242.00', 0, '0.0000', 0, '', '0.00', '2017-06-06 11:50:57', '2017-06-06 11:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_cart_items`
--

CREATE TABLE `cc_cart_items` (
  `cart_item_id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `sku` varchar(127) DEFAULT NULL,
  `costume_name` varchar(255) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `price` float NOT NULL DEFAULT '0',
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_cart_items`
--

INSERT INTO `cc_cart_items` (`cart_item_id`, `costume_id`, `sku`, `costume_name`, `qty`, `price`, `cart_id`) VALUES
(461, 107, 'CS000001', 'jithender test1-1', 1, 122, 135),
(462, 109, 'CS000003', 'jithendertest1-3', 1, 10, 135),
(463, 110, 'CS000004', 'jithendertest2-1', 1, 100, 135),
(464, 111, 'CS000005', 'jithendertest2-2', 1, 10, 135);

-- --------------------------------------------------------

--
-- Table structure for table `cc_category`
--

CREATE TABLE `cc_category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `thumb_image` varchar(127) DEFAULT NULL,
  `banner_image` varchar(127) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_category`
--

INSERT INTO `cc_category` (`category_id`, `name`, `description`, `parent_id`, `thumb_image`, `banner_image`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(66, 'Men''s', 'Men''s', 0, 'fh3FQ2pktV.jpg', 'AEdOr8GVTX.jpg', 0, 1, '2017-04-12 10:58:43', '2017-05-29 00:44:54'),
(67, 'Women''s', 'Women''s', 0, 'pjFvYA8c7R.jpg', 'mfX0MaVglz.jpg', 0, 1, '2017-04-12 10:59:25', '2017-05-29 00:48:36'),
(68, 'Kid''s', 'Kid''s', 0, '1d7NrokGDi.jpg', 'W39CEX5Kul.jpg', 0, 1, '2017-04-12 10:59:47', '2017-05-29 00:48:47'),
(74, 'Pets', 'pets', 0, 'JnAQM8Jx2F.jpg', 'ut7wyo8o0q.png', 0, 1, '2017-04-21 02:54:27', '0000-00-00 00:00:00'),
(75, 'Holiday', 'Holiday', 0, 'oSkZzX6nSg.jpg', 'QPcrhR3L1q.png', 0, 1, '2017-04-21 02:55:05', '2017-05-24 06:22:20'),
(76, 'Cosplay', 'Cosplay', 0, 'H620Umx550.jpg', 'H84zgkrCFs.png', 0, 1, '2017-04-21 02:55:40', '0000-00-00 00:00:00'),
(78, 'Film & Theatre', 'Film & Theatre', 0, 'CRCzgv45sb.jpg', '3OmBjTsAly.png', 0, 1, '2017-04-21 02:57:11', '0000-00-00 00:00:00'),
(79, 'Animals & Insects', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the ', 66, 'B0coty33GJ.png', 'dNSCOtM5QJ.jpg', 0, 1, '2017-04-21 03:38:43', '2017-05-24 09:53:11'),
(80, 'TV & Movie', 'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 66, 'o9wZavwWJE.jpg', 'NHtly9xJJJ.jpg', 0, 1, '2017-04-21 03:40:19', '2017-05-31 02:31:04'),
(81, 'Superheroes & Villains', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ', 66, 'AFcadxSZLP.jpg', 'gbyA3ff8kl.jpg', 0, 1, '2017-04-21 03:43:54', '0000-00-00 00:00:00'),
(82, 'Horror', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 66, '2oKCD8TMtK.jpg', 'viRPeuzukX.jpg', 0, 1, '2017-04-21 03:44:54', '0000-00-00 00:00:00'),
(83, 'Storybook & Fairytale', 'Storybook & Fairytale', 66, '28uk4OKY26.jpg', 'PZzpulRnks.png', 0, 1, '2017-04-21 03:45:45', '0000-00-00 00:00:00'),
(84, 'Historical Figures', 'Historical Figures', 66, 'UICAFP6FmF.jpg', '6nQC3mqCjE.jpg', 0, 1, '2017-04-21 03:46:42', '0000-00-00 00:00:00'),
(85, 'Groups & Couples', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 66, 'T5J61FXQ1e.jpg', 'apQOsCDMez.png', 0, 1, '2017-04-21 03:48:19', '0000-00-00 00:00:00'),
(86, 'Miscellaneous', 'Miscellaneous', 66, 'pKWpcX3p75.jpg', 'qwqjSHkgmJ.png', 0, 1, '2017-04-21 03:49:04', '0000-00-00 00:00:00'),
(87, 'Animals & Insects', 'Animals & Insects', 67, 'PwUO95ZRyN.jpg', 'NiMlEqU5QG.jpg', 0, 1, '2017-04-21 03:50:04', '2017-05-31 02:30:34'),
(88, 'TV & Movie', 'TV & Movie', 67, 'cLcNJEfwXA.jpg', 'NJIZzqD0kC.png', 0, 1, '2017-04-21 03:50:33', '0000-00-00 00:00:00'),
(89, 'Superheroes & Villains', 'Superheroes & Villains', 67, 't0WqAZ7pBJ.jpg', 'qYVTemQJFt.png', 0, 1, '2017-04-21 05:06:27', '0000-00-00 00:00:00'),
(90, 'Horror', 'Horror', 67, 'VcMokESdY7.jpg', '81llJGd21p.png', 0, 1, '2017-04-21 05:08:13', '0000-00-00 00:00:00'),
(91, 'Storybook & Fairytale', 'Storybook & Fairytale', 67, 'rVThhiZES7.jpg', 'iqSdLkbW6R.jpg', 0, 1, '2017-04-21 05:08:55', '0000-00-00 00:00:00'),
(92, 'Historical Figures', 'Historical Figures', 67, 'vVyTxLElyD.jpg', 'RHFgfYwFYf.jpg', 0, 1, '2017-04-21 05:09:21', '0000-00-00 00:00:00'),
(93, 'Groups & Couples', 'Groups & Couples', 67, 'G4HFOYabRJ.jpg', 'FsgrlMPLSX.png', 0, 1, '2017-04-21 05:09:43', '0000-00-00 00:00:00'),
(95, 'Miscellaneous', 'Miscellaneous', 67, 'DXqQDAi1oA.jpg', 'jiCkifvrH9.png', 0, 1, '2017-04-21 05:10:56', '0000-00-00 00:00:00'),
(97, 'Anime/Manga', 'Anime/Manga', 76, 'hZJLDrrl2p.jpg', 'f9rrRkTPKl.jpg', 0, 1, '2017-04-24 01:29:17', '0000-00-00 00:00:00'),
(98, 'Comic/Superhero', 'Comic/Superhero', 76, 'ddxnp5IKdX.jpg', 'ltIoFKFlAz.jpg', 0, 1, '2017-04-24 01:30:03', '2017-05-05 06:19:44'),
(99, 'Furries', 'Furries', 76, 'AKeoGnaBIA.jpg', 'qlDfmWIrlw.jpg', 0, 1, '2017-04-24 01:30:35', '0000-00-00 00:00:00'),
(100, 'Film & TV', 'Film & TV', 76, 'XznnpkWw3n.jpg', 'nrmEfcB97H.jpg', 0, 1, '2017-04-24 01:31:02', '0000-00-00 00:00:00'),
(101, 'Mecha', 'Mecha', 76, 'oKoDHOKrkh.jpg', 'Ib8MHocwtB.jpg', 0, 1, '2017-04-24 01:31:34', '0000-00-00 00:00:00'),
(102, 'Sci-Fi', 'Sci-Fi', 76, 'QTJ06AA9Aa.jpg', 'f1YbFE2EYq.jpg', 0, 1, '2017-04-24 01:31:59', '0000-00-00 00:00:00'),
(103, 'Video Games', 'Video Games', 76, 'K8huHmlCPy.jpg', '2IJQ6pZA34.jpg', 0, 1, '2017-04-24 01:32:28', '0000-00-00 00:00:00'),
(114, 'Other', 'Other', 76, 'aOSyzYBEpI.jpg', 'eI38Tvz8bX.png', 0, 1, '2017-04-24 01:38:13', '0000-00-00 00:00:00'),
(116, 'Cyberpunk', 'Cyberpunk', 77, 'JMa514nBpz.jpg', 'JYuyT1cLXW.jpg', 0, 1, '2017-04-24 06:33:19', '0000-00-00 00:00:00'),
(117, 'Dystopian', 'Dystopian', 77, 'UpjkR8EykT.jpg', 'JLIuArubWN.jpg', 0, 1, '2017-04-24 06:34:06', '0000-00-00 00:00:00'),
(119, 'Steampunk', 'Steampunk', 77, '2KzqeJsgkJ.jpg', 'puxS7QZjUc.jpg', 0, 1, '2017-04-24 06:34:56', '0000-00-00 00:00:00'),
(120, 'Streetwear', 'Streetwear', 77, '4fKTCx6pef.jpg', 'nui0aDB3mu.jpg', 0, 1, '2017-04-24 06:35:33', '2017-04-26 05:46:49'),
(121, 'Lolita', 'Lolita', 77, '0so1Vk4rkx.jpg', 'jnXfNlQ159.jpg', 0, 1, '2017-04-24 06:35:58', '0000-00-00 00:00:00'),
(122, 'Mori Kei', 'Lolita', 77, 'ScdyrOaILd.jpg', 'KZVNJJnzPy.jpg', 0, 1, '2017-04-24 06:36:28', '0000-00-00 00:00:00'),
(123, 'Fairy Kei', 'Fairy Kei', 77, 'k3g6sjWkqc.jpg', '8WJeyZIn9y.jpg', 0, 1, '2017-04-24 06:36:55', '0000-00-00 00:00:00'),
(124, 'Visual Kei', 'Visual Kei', 77, 'MWdXZRvWWI.jpg', 'RSNs7YIpbR.jpg', 0, 1, '2017-04-24 06:37:21', '0000-00-00 00:00:00'),
(125, 'Other', 'Other', 77, 't9FvC3NZGq.jpg', 'UGtmJTqc0c.jpg', 0, 1, '2017-04-24 06:37:43', '0000-00-00 00:00:00'),
(126, 'Circus', 'Circus', 78, 'fsE1eRrZku.jpg', 'HAqbUzgNmp.png', 0, 1, '2017-04-24 06:38:24', '0000-00-00 00:00:00'),
(127, 'Historical Reenactments', 'Historical Reenactments', 78, 'mgsPQdYlkQ.jpg', 'X20DBKUQ6G.jpg', 0, 1, '2017-04-24 06:39:33', '0000-00-00 00:00:00'),
(128, 'LARP', 'LARP', 78, 'nQDXcbtsGh.jpg', '67WqikKxi7.jpg', 0, 1, '2017-04-24 06:39:58', '0000-00-00 00:00:00'),
(129, 'Masquerade', 'Masquerade', 78, 'CIzcRTuPUs.jpg', 'jofHN2DueV.jpg', 0, 1, '2017-04-24 06:40:35', '0000-00-00 00:00:00'),
(130, 'Medieval/Renaissance Fairs', 'Medieval/Renaissance Fairs', 78, 'WZVcV06iNX.jpg', 'VEcxyyS9iy.jpg', 0, 1, '2017-04-24 06:41:44', '0000-00-00 00:00:00'),
(131, 'Theatre', 'Theatre', 78, 'QVtVzGevoN.jpg', 'jhDjsAvd2t.jpg', 0, 1, '2017-04-24 06:42:25', '0000-00-00 00:00:00'),
(132, 'Music Videos', 'Music Videos', 78, 'bxZFFlMtlT.jpg', 'E6WPO88Yqx.jpg', 0, 1, '2017-04-24 06:42:51', '0000-00-00 00:00:00'),
(137, 'Vehicles', 'Description for the vehicles', 78, '1hXhMT8zpy.jpg', 'hP7TdTSc2N.jpg', 0, 1, '2017-05-03 05:45:29', '0000-00-00 00:00:00'),
(138, 'Test', 'Testing', 67, 'EEmlZUloap.jpg', 'Bt3CjkZv5T.jpg', 0, 1, '2017-05-03 06:08:49', '0000-00-00 00:00:00'),
(139, 'Hallocks', 'Hallocks test', 74, '6NHrURQYYM.png', 'hoDadxrvNG.jpg', 0, 1, '2017-05-03 08:04:07', '2017-05-25 08:49:59'),
(140, 'Kensel', 'Kenel category ', 66, 'lrvmLLabdT.jpg', 'G0Rska2Gqx.jpg', 0, 1, '2017-05-03 09:06:45', '0000-00-00 00:00:00'),
(141, 'Goth', 'Testing ', 77, 'izM9iOTnl9.jpg', 'Poq0pjWdHu.jpg', 0, 1, '2017-05-04 05:58:45', '0000-00-00 00:00:00'),
(142, 'Goth', 'Testing', 77, 'f2ptnGhSZb.jpg', '6da6a0fneu.jpg', 0, 1, '2017-05-04 06:00:36', '0000-00-00 00:00:00'),
(143, 'Unique Fashion', 'Testing the categories ', 0, 'r7ndvASV08.jpg', 'YxxF4G3H2A.jpg', 0, 1, '2017-05-05 02:47:58', '0000-00-00 00:00:00'),
(144, 'Fashion Costumes', 'Costume made with special accessories ', 143, 'wJ8JfzaTp9.jpg', 'Q69KWVlkXm.jpg', 0, 1, '2017-05-05 02:48:56', '0000-00-00 00:00:00'),
(145, 'Kids Costumes ', 'Costumes for kids ', 66, '24hfE7LpGs.jpg', 'fu6DS1gxOm.jpg', 0, 1, '2017-05-16 00:44:12', '0000-00-00 00:00:00'),
(146, 'New Costumes', 'Costumes for women''s of the latest costumes ', 67, 'mmrReMuA86.jpg', 'sQZWR3pDWP.jpg', 0, 1, '2017-05-29 05:50:09', '2017-05-29 08:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `cc_category_description`
--

CREATE TABLE `cc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_charities`
--

CREATE TABLE `cc_charities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `suggested_by` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=REDUNDANT;

--
-- Dumping data for table `cc_charities`
--

INSERT INTO `cc_charities` (`id`, `name`, `suggested_by`, `image`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Coral Reef Alliance', 1, 'Egf6ILOdEU.png', '0', '2017-04-20 01:53:39', '2017-05-16 00:00:00'),
(7, 'Ocean Conservaancy', 1, 'yQVTwNy6Ad.png', '0', '2017-04-20 01:54:50', '2017-05-16 00:00:00'),
(10, 'Rainforest Alliance', 1, 'VIQh82ZZRB.png', '1', '2017-04-20 01:56:52', '2017-05-16 00:00:00'),
(11, 'Wild Aid', 1, 'duXAaNJy1j.png', '1', '2017-04-20 01:57:18', '2017-05-16 00:00:00'),
(20, 'The Life You Can Save', 1, '1gvBfXmoD3.png', '1', '2017-05-16 01:43:40', '2017-05-16 00:00:00'),
(21, 'Natural Resources Defence Council', 1, 'fEDcLKpgyO.png', '1', '2017-05-16 01:44:18', '2017-05-16 01:44:18');

-- --------------------------------------------------------

--
-- Table structure for table `cc_cms_blocks`
--

CREATE TABLE `cc_cms_blocks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_cms_pages`
--

CREATE TABLE `cc_cms_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(127) DEFAULT NULL,
  `meta_desc` varchar(511) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_cms_pages`
--

INSERT INTO `cc_cms_pages` (`id`, `title`, `url`, `description`, `meta_title`, `meta_desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About US', 'http://chrysalisqa.local.dotcomweavers.net/about-us', '<p>gogole me</p>', 'google', 'googlew nme', 1, '2017-05-26 03:47:51', '2017-05-26 03:47:51'),
(3, 'about', 'http://chrysalisqa.local.dotcomweavers.net', '<p>    about<br></p>', 'Title', 'Description', 1, '2017-05-26 05:33:52', '2017-05-26 05:33:52'),
(4, 'Chrysalis testing', 'http://chrysalisqa.local.dotcomweavers.net/chrysalis.com', '<p>Test Description&nbsp;&nbsp;&nbsp;&nbsp;<br></p>', 'Costumes list ', 'Test Description ', 1, '2017-05-29 01:21:27', '2017-05-29 01:21:27'),
(5, 'New Static page', 'http://chrysalisqa.local.dotcomweavers.net/www.lenskart.com', '<p>Test Description&nbsp;&nbsp;&nbsp;&nbsp;<br></p>', 'Testing', 'Test Description ', 1, '2017-05-29 06:13:59', '2017-05-29 06:13:59'),
(6, 'Contact Us', 'http://chrysalisqa.local.dotcomweavers.net/chrysalis.com/contact us', '<p>contact us page description ,about the information&nbsp;<strong style="text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin: 0px; padding: 0px;">Lorem Ipsum</strong><span style="text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">&nbsp;</span><span style="text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', 'About us ', 'About us contact us page', 1, '2017-05-31 02:24:00', '2017-05-31 02:24:00'),
(7, 'Testing', 'http://chrysalisqa.local.dotcomweavers.net/ch', '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span class="Apple-converted-space"> </span>is\r\n simply dummy text of the printing and typesetting industry. Lorem Ipsum\r\n has been the industry''s standard dummy text ever since the 1500s, when \r\nan unknown printer took a galley of type and scrambled it to make a type\r\n specimen book. It has survived not only five centuries, but also the \r\nleap into electronic typesetting, remaining essentially unchanged. It \r\nwas popularised in the 1960s with the release of Letraset sheets \r\ncontaining Lorem Ipsum passages, and more recently with desktop \r\npublishing software like Aldus PageMaker including versions of Lorem \r\nIpsum.</span><br></p>', 'Test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also', 1, '2017-06-01 01:48:31', '2017-06-01 01:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `cc_conversations`
--

CREATE TABLE `cc_conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('request_a_bag','order','support') COLLATE utf8_unicode_ci NOT NULL,
  `user_one` int(11) NOT NULL,
  `user_two` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cc_conversations`
--

INSERT INTO `cc_conversations` (`id`, `type`, `user_one`, `user_two`, `status`, `created_at`, `updated_at`) VALUES
(1, 'request_a_bag', 12, 1, 0, '2017-05-03 21:54:00', NULL),
(2, 'request_a_bag', 12, 1, 0, '2017-05-03 22:08:17', NULL),
(3, 'request_a_bag', 1, 1, 0, '2017-05-03 23:51:37', '2017-05-30 21:46:35'),
(4, 'request_a_bag', 1, 1, 0, '2017-05-04 20:20:26', NULL),
(5, 'request_a_bag', 24, 1, 0, '2017-05-05 02:42:04', NULL),
(6, 'request_a_bag', 24, 1, 0, '2017-05-05 02:42:20', NULL),
(7, 'request_a_bag', 11, 1, 0, '2017-05-05 02:50:18', NULL),
(8, 'request_a_bag', 24, 1, 0, '2017-05-05 03:41:17', NULL),
(9, 'request_a_bag', 25, 1, 0, '2017-05-05 03:45:50', NULL),
(10, 'request_a_bag', 24, 1, 0, '2017-05-05 03:47:55', NULL),
(11, 'request_a_bag', 24, 1, 0, '2017-05-05 03:58:47', NULL),
(12, 'request_a_bag', 24, 1, 0, '2017-05-05 04:06:42', NULL),
(13, 'request_a_bag', 24, 1, 0, '2017-05-05 04:17:37', NULL),
(14, 'request_a_bag', 1, 1, 0, '2017-05-05 19:04:33', NULL),
(15, 'request_a_bag', 24, 1, 0, '2017-05-05 21:50:00', NULL),
(16, 'request_a_bag', 24, 1, 0, '2017-05-06 00:15:49', NULL),
(17, 'request_a_bag', 24, 1, 0, '2017-05-06 00:39:36', NULL),
(18, 'request_a_bag', 1, 1, 0, '2017-05-07 21:21:29', NULL),
(19, 'request_a_bag', 27, 1, 0, '2017-05-07 22:00:46', NULL),
(20, 'request_a_bag', 24, 1, 0, '2017-05-07 22:06:50', NULL),
(21, 'request_a_bag', 27, 1, 0, '2017-05-07 22:20:19', NULL),
(22, 'request_a_bag', 1, 1, 0, '2017-05-08 00:19:05', NULL),
(23, 'request_a_bag', 12, 1, 0, '2017-05-08 01:51:48', NULL),
(24, 'request_a_bag', 12, 1, 0, '2017-05-08 02:57:37', NULL),
(25, 'request_a_bag', 1, 1, 0, '2017-05-08 03:36:58', NULL),
(26, 'request_a_bag', 24, 1, 0, '2017-05-08 04:00:26', NULL),
(27, 'request_a_bag', 24, 1, 0, '2017-05-08 19:46:04', NULL),
(28, 'request_a_bag', 1, 1, 0, '2017-05-08 21:42:44', NULL),
(29, 'request_a_bag', 1, 1, 0, '2017-05-08 21:43:00', NULL),
(30, 'request_a_bag', 1, 1, 0, '2017-05-08 21:45:30', NULL),
(31, 'request_a_bag', 24, 1, 0, '2017-05-08 22:31:04', NULL),
(32, 'request_a_bag', 1, 1, 0, '2017-05-09 01:12:34', NULL),
(33, 'request_a_bag', 24, 1, 0, '2017-05-09 02:00:33', NULL),
(34, 'request_a_bag', 11, 1, 0, '2017-05-09 02:43:08', NULL),
(35, 'request_a_bag', 24, 1, 0, '2017-05-09 02:56:27', NULL),
(36, 'request_a_bag', 12, 1, 0, '2017-05-09 03:17:29', NULL),
(37, 'request_a_bag', 24, 1, 0, '2017-05-09 03:38:48', NULL),
(38, 'request_a_bag', 24, 1, 0, '2017-05-11 00:56:46', NULL),
(39, 'request_a_bag', 24, 1, 0, '2017-05-11 01:04:33', NULL),
(40, 'request_a_bag', 1, 34, 1, '2017-05-23 01:52:20', '2017-05-23 01:52:21'),
(41, 'request_a_bag', 34, 34, 1, '2017-05-24 21:11:00', '2017-06-01 00:07:45'),
(42, 'order', 40, 38, 1, '2017-05-26 00:29:34', NULL),
(43, 'order', 40, 24, 1, '2017-05-26 00:32:53', NULL),
(45, 'order', 42, 34, 1, '2017-05-26 22:07:54', NULL),
(46, 'order', 42, 24, 1, '2017-05-26 22:07:56', NULL),
(47, 'request_a_bag', 41, 1, 0, '2017-05-28 20:13:33', NULL),
(48, 'order', 41, 42, 1, '2017-05-28 20:41:17', NULL),
(49, 'request_a_bag', 41, 1, 0, '2017-05-28 22:16:45', NULL),
(50, 'request_a_bag', 41, 1, 0, '2017-05-28 23:51:53', NULL),
(51, 'order', 41, 40, 1, '2017-05-29 02:47:59', NULL),
(52, 'order', 41, 38, 1, '2017-05-29 04:09:51', NULL),
(53, 'order', 41, 24, 1, '2017-05-29 04:09:53', NULL),
(54, 'order', 41, 42, 1, '2017-05-29 04:09:59', NULL),
(60, 'request_a_bag', 44, 44, 1, '2017-05-29 21:08:12', '2017-05-29 21:23:04'),
(61, 'request_a_bag', 44, 1, 1, '2017-05-29 21:21:21', NULL),
(66, 'request_a_bag', 43, 1, 1, '2017-05-30 21:17:33', NULL),
(67, 'request_a_bag', 43, 43, 1, '2017-05-30 21:19:22', '2017-05-30 21:35:49'),
(68, 'support', 43, 2, 0, NULL, NULL),
(69, 'support', 43, 2, 0, NULL, NULL),
(70, 'request_a_bag', 1, 1, 1, '2017-05-31 22:04:54', NULL),
(72, 'order', 42, 34, 1, '2017-05-31 23:54:14', NULL),
(73, 'order', 42, 24, 1, '2017-05-31 23:54:15', NULL),
(74, 'request_a_bag', 43, 1, 1, '2017-06-01 00:34:02', NULL),
(75, 'support', 43, 2, 0, NULL, NULL),
(76, 'support', 43, 2, 0, NULL, NULL),
(77, 'support', 43, 2, 0, NULL, NULL),
(78, 'support', 43, 2, 0, NULL, NULL),
(79, 'support', 43, 2, 0, NULL, NULL),
(80, 'support', 43, 2, 0, NULL, NULL),
(81, 'support', 43, 2, 0, NULL, NULL),
(82, 'support', 43, 2, 0, NULL, NULL),
(83, 'support', 43, 2, 0, NULL, NULL),
(84, 'support', 1, 2, 0, NULL, NULL),
(85, 'support', 66, 2, 0, NULL, NULL),
(86, 'support', 1, 2, 0, NULL, NULL),
(87, 'support', 1, 2, 0, NULL, NULL),
(88, 'support', 1, 2, 0, NULL, NULL),
(89, 'support', 1, 2, 0, NULL, NULL),
(90, 'order', 44, 42, 1, '2017-06-01 04:30:10', NULL),
(91, 'order', 44, 42, 1, '2017-06-01 04:36:53', NULL),
(92, 'order', 42, 34, 1, '2017-06-01 07:24:55', NULL),
(93, 'order', 42, 37, 1, '2017-06-01 07:24:57', NULL),
(94, 'order', 42, 35, 1, '2017-06-01 07:24:59', NULL),
(95, 'order', 42, 42, 1, '2017-06-01 07:25:01', NULL),
(96, 'order', 42, 42, 1, '2017-05-31 19:41:57', NULL),
(97, 'order', 42, 37, 1, '2017-05-31 19:41:58', NULL),
(98, 'order', 42, 35, 1, '2017-05-31 19:42:00', NULL),
(99, 'order', 42, 34, 1, '2017-05-31 19:42:02', NULL),
(100, 'support', 43, 2, 0, NULL, NULL),
(101, 'order', 68, 34, 1, '2017-06-02 05:32:52', NULL),
(102, 'order', 68, 1, 1, '2017-06-02 05:32:54', NULL),
(103, 'order', 68, 34, 1, '2017-06-02 23:25:26', NULL),
(104, 'order', 68, 1, 1, '2017-06-02 23:25:28', NULL),
(105, 'order', 68, 34, 1, '2017-06-02 23:49:24', NULL),
(106, 'order', 68, 34, 1, '2017-06-02 23:52:10', NULL),
(107, 'order', 68, 1, 1, '2017-06-02 23:52:13', NULL),
(108, 'order', 68, 34, 1, '2017-06-03 01:22:47', NULL),
(109, 'order', 68, 1, 1, '2017-06-03 01:22:49', NULL),
(110, 'order', 68, 1, 1, '2017-06-03 01:29:00', NULL),
(111, 'order', 68, 1, 1, '2017-06-02 23:36:40', NULL),
(112, 'order', 68, 34, 1, '2017-06-02 23:36:43', NULL),
(113, 'order', 68, 34, 1, '2017-06-02 23:53:04', NULL),
(114, 'order', 68, 1, 1, '2017-06-02 23:53:07', NULL),
(115, 'order', 68, 34, 1, '2017-06-03 00:12:31', NULL),
(116, 'order', 68, 1, 1, '2017-06-03 00:12:34', NULL),
(117, 'order', 85, 34, 1, '2017-06-04 22:07:43', NULL),
(118, 'order', 85, 34, 1, '2017-06-04 22:08:06', NULL),
(119, 'order', 85, 34, 1, '2017-06-04 22:09:16', NULL),
(120, 'order', 85, 34, 1, '2017-06-04 22:09:35', NULL),
(121, 'order', 85, 34, 1, '2017-06-04 22:11:50', NULL),
(122, 'order', 85, 34, 1, '2017-06-04 22:12:17', NULL),
(123, 'order', 85, 34, 1, '2017-06-04 22:12:33', NULL),
(124, 'order', 85, 34, 1, '2017-06-04 22:13:09', NULL),
(125, 'order', 85, 34, 1, '2017-06-04 22:13:53', NULL),
(126, 'order', 85, 34, 1, '2017-06-04 22:14:10', NULL),
(127, 'order', 85, 34, 1, '2017-06-04 22:14:27', NULL),
(128, 'order', 85, 34, 1, '2017-06-04 22:14:42', NULL),
(129, 'order', 85, 34, 1, '2017-06-04 22:14:59', NULL),
(130, 'order', 85, 34, 1, '2017-06-04 22:16:55', NULL),
(131, 'order', 85, 34, 1, '2017-06-04 22:17:24', NULL),
(132, 'order', 85, 34, 1, '2017-06-04 22:17:56', NULL),
(133, 'order', 85, 34, 1, '2017-06-04 22:20:21', NULL),
(134, 'order', 85, 34, 1, '2017-06-04 22:21:28', NULL),
(135, 'order', 85, 34, 1, '2017-06-04 22:22:34', NULL),
(136, 'order', 85, 34, 1, '2017-06-04 22:23:49', NULL),
(137, 'order', 88, 87, 1, '2017-06-06 04:41:53', NULL),
(138, 'order', 88, 87, 1, '2017-06-06 04:49:26', NULL),
(139, 'order', 88, 87, 1, '2017-06-06 04:59:03', NULL),
(140, 'order', 88, 87, 1, '2017-06-06 05:08:17', NULL),
(141, 'order', 88, 87, 1, '2017-06-06 05:11:45', NULL),
(142, 'order', 88, 86, 1, '2017-06-06 05:11:47', NULL),
(143, 'order', 88, 86, 1, '2017-06-06 06:02:22', NULL),
(144, 'order', 88, 87, 1, '2017-06-06 06:02:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cc_costumes`
--

CREATE TABLE `cc_costumes` (
  `costume_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku_no` varchar(63) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `gender` enum('male','female','unisex','pets') DEFAULT NULL,
  `condition` enum('brand_new','good','like_new','excellent') DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_user_group` enum('admin','user') NOT NULL DEFAULT 'admin',
  `created_by` bigint(20) NOT NULL,
  `viewed` int(5) NOT NULL DEFAULT '0',
  `item_location` int(11) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `size` enum('1sz','xxs','xs','s','m','l','xl','xxl') DEFAULT NULL,
  `charity_id` int(11) NOT NULL,
  `donation_amount` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('1','0') NOT NULL DEFAULT '0',
  `weight_pounds` varchar(100) NOT NULL DEFAULT '0',
  `weight_ounces` varchar(100) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL,
  `dynamic_percent` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_costumes`
--

INSERT INTO `cc_costumes` (`costume_id`, `name`, `sku_no`, `quantity`, `price`, `gender`, `condition`, `sort_order`, `status`, `created_user_group`, `created_by`, `viewed`, `item_location`, `address_id`, `size`, `charity_id`, `donation_amount`, `created_at`, `updated_at`, `deleted_status`, `weight_pounds`, `weight_ounces`, `cat_id`, `dynamic_percent`) VALUES
(107, '', 'CS000001', 10, '122.0000', 'male', '', 0, 'active', 'user', 86, 0, 0, 261, 'xxl', 0, 0, '2017-06-06 07:43:01', '2017-06-06 07:43:03', '0', '500', '0', 66, NULL),
(108, '', 'CS000002', 7, '212.0000', '', '', 0, 'active', 'user', 86, 0, 3305, 262, 'xl', 0, 0, '2017-06-06 07:44:16', '2017-06-06 07:44:18', '0', '12', '21', 66, NULL),
(109, '', 'CS000003', 3, '10.0000', 'unisex', '', 0, 'active', 'user', 86, 0, 2420, 263, 'xs', 0, 0, '2017-06-06 07:45:41', '2017-06-06 07:45:43', '0', '12', '21', 66, NULL),
(110, '', 'CS000004', 4, '100.0000', 'female', '', 0, 'active', 'user', 87, 0, 11238, 264, 'xs', 0, 0, '2017-06-06 07:47:25', '2017-06-06 07:47:27', '0', '12', '21', 66, NULL),
(111, '', 'CS000005', 3, '10.0000', 'female', '', 0, 'active', 'user', 87, 0, 2420, 265, 'xs', 10, 10, '2017-06-06 07:48:39', '2017-06-06 07:48:41', '0', '10', '21', 66, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cc_costumes_like`
--

CREATE TABLE `cc_costumes_like` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_costume_attribute_options`
--

CREATE TABLE `cc_costume_attribute_options` (
  `id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_option_value_id` int(11) NOT NULL,
  `attribute_option_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_costume_attribute_options`
--

INSERT INTO `cc_costume_attribute_options` (`id`, `costume_id`, `attribute_id`, `attribute_option_value_id`, `attribute_option_value`) VALUES
(1798, 107, 16, 0, '12'),
(1799, 107, 17, 0, '21'),
(1800, 107, 18, 0, '12'),
(1801, 107, 19, 0, '21'),
(1802, 107, 20, 0, '12'),
(1803, 107, 2, 8, 'No'),
(1804, 107, 3, 10, 'No'),
(1805, 107, 4, 12, 'No'),
(1806, 107, 5, 31, 'No'),
(1807, 107, 21, 33, 'No'),
(1808, 107, 6, 0, 'Describe your Costume: Including accessories'),
(1809, 107, 7, 0, 'Fun Fact: A little backstory to your costume and the adventures it has experienced'),
(1810, 107, 8, 0, ' FAQ Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox'),
(1811, 107, 22, 0, '12'),
(1812, 107, 23, 0, '12'),
(1813, 107, 24, 0, '12'),
(1814, 107, 14, 26, 'Same Business Day'),
(1815, 107, 15, 28, 'Return Accepted'),
(1816, 108, 16, 0, '12'),
(1817, 108, 17, 0, '21'),
(1818, 108, 18, 0, '12'),
(1819, 108, 19, 0, '21'),
(1820, 108, 20, 0, '12'),
(1821, 108, 2, 8, 'No'),
(1822, 108, 3, 10, 'No'),
(1823, 108, 4, 12, 'No'),
(1824, 108, 5, 31, 'No'),
(1825, 108, 21, 33, 'No'),
(1826, 108, 6, 0, 'Describe your Costume: Including accessories'),
(1827, 108, 7, 0, ' Fun Fact: A little backstory to your costume and the adventures it has experienced'),
(1828, 108, 8, 0, 'FAQ Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox'),
(1829, 108, 22, 0, '12'),
(1830, 108, 23, 0, '21'),
(1831, 108, 24, 0, '12'),
(1832, 108, 14, 26, 'Same Business Day'),
(1833, 108, 15, 29, 'Return Not Accepted '),
(1834, 109, 16, 0, '12'),
(1835, 109, 17, 0, '21'),
(1836, 109, 18, 0, '12'),
(1837, 109, 19, 0, '12'),
(1838, 109, 20, 0, '21'),
(1839, 109, 2, 8, 'No'),
(1840, 109, 3, 10, 'No'),
(1841, 109, 4, 12, 'No'),
(1842, 109, 5, 31, 'No'),
(1843, 109, 21, 33, 'No'),
(1844, 109, 6, 0, 'Describe your Costume: Including accessories'),
(1845, 109, 7, 0, 'Fun Fact: A little backstory to your costume and the adventures it has experienced'),
(1846, 109, 8, 0, 'FAQ Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox'),
(1847, 109, 22, 0, '12'),
(1848, 109, 23, 0, '21'),
(1849, 109, 24, 0, '12'),
(1850, 109, 14, 27, '10 Business Days'),
(1851, 109, 15, 29, 'Return Not Accepted '),
(1852, 110, 16, 0, '12'),
(1853, 110, 17, 0, '21'),
(1854, 110, 18, 0, '12'),
(1855, 110, 19, 0, '21'),
(1856, 110, 20, 0, '12'),
(1857, 110, 2, 8, 'No'),
(1858, 110, 3, 10, 'No'),
(1859, 110, 4, 12, 'No'),
(1860, 110, 5, 31, 'No'),
(1861, 110, 21, 33, 'No'),
(1862, 110, 6, 0, 'Describe your Costume: Including accessories'),
(1863, 110, 7, 0, 'Fun Fact: A little backstory to your costume and the adventures it has experienced'),
(1864, 110, 8, 0, 'FAQ Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox'),
(1865, 110, 22, 0, '12'),
(1866, 110, 23, 0, '21'),
(1867, 110, 24, 0, '12'),
(1868, 110, 14, 27, '10 Business Days'),
(1869, 110, 15, 29, 'Return Not Accepted '),
(1870, 111, 16, 0, '12'),
(1871, 111, 17, 0, '21'),
(1872, 111, 18, 0, '12'),
(1873, 111, 19, 0, '21'),
(1874, 111, 20, 0, '12'),
(1875, 111, 2, 8, 'No'),
(1876, 111, 3, 10, 'No'),
(1877, 111, 4, 12, 'No'),
(1878, 111, 5, 31, 'No'),
(1879, 111, 21, 33, 'No'),
(1880, 111, 6, 0, 'Describe your Costume: Including accessories'),
(1881, 111, 7, 0, 'Fun Fact: A little backstory to your costume and the adventures it has experienced'),
(1882, 111, 8, 0, 'FAQ Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox'),
(1883, 111, 22, 0, '12'),
(1884, 111, 23, 0, '21'),
(1885, 111, 24, 0, '12'),
(1886, 111, 14, 26, 'Same Business Day'),
(1887, 111, 15, 28, 'Return Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `cc_costume_description`
--

CREATE TABLE `cc_costume_description` (
  `costume_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_costume_description`
--

INSERT INTO `cc_costume_description` (`costume_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`) VALUES
(107, 1, 'jithender test1-1', 'Describe your Costume: Including accessories', '', ''),
(108, 1, 'jithendertest1-2', 'Describe your Costume: Including accessories', '', ''),
(109, 1, 'jithendertest1-3', 'Describe your Costume: Including accessories', '', ''),
(110, 1, 'jithendertest2-1', 'Describe your Costume: Including accessories', '', ''),
(111, 1, 'jithendertest2-2', 'Describe your Costume: Including accessories', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cc_costume_image`
--

CREATE TABLE `cc_costume_image` (
  `costume_image_id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_costume_image`
--

INSERT INTO `cc_costume_image` (`costume_image_id`, `costume_id`, `image`, `type`, `sort_order`) VALUES
(281, 107, 'FJtrSKB5XL.jpg', 1, 0),
(282, 107, 'qfsvRIzeke.jpg', 2, 0),
(283, 107, 'rV8jyaoOs6.png', 3, 0),
(284, 108, 'P2qnEKYAVb.jpg', 1, 0),
(285, 108, 'UYTn7SqKc4.jpg', 2, 0),
(286, 108, '9hZskyrFjO.png', 3, 0),
(287, 109, 'CWZKYYrWkm.png', 1, 0),
(288, 109, 'hhr6duShbs.jpg', 2, 0),
(289, 109, 'VqR8aJHB5f.png', 3, 0),
(290, 110, 'G3vOaqxvJw.jpg', 1, 0),
(291, 110, 'MPgI4qiqLY.png', 2, 0),
(292, 110, 'in5bGcFekR.jpg', 3, 0),
(293, 111, 'hMO85Bbv92.jpg', 1, 0),
(294, 111, '8Sp1vXUBeV.jpg', 2, 0),
(295, 111, 'ltD6lmofCC.png', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cc_costume_to_category`
--

CREATE TABLE `cc_costume_to_category` (
  `costume_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_no` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_costume_to_category`
--

INSERT INTO `cc_costume_to_category` (`costume_id`, `category_id`, `sort_no`) VALUES
(107, 79, 1),
(108, 82, 1),
(109, 79, 1),
(110, 79, 1),
(111, 79, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cc_countries`
--

CREATE TABLE `cc_countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_countries`
--

INSERT INTO `cc_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People''s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'YU', 'Yugoslavia'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `cc_coupon_category`
--

CREATE TABLE `cc_coupon_category` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_coupon_costumes`
--

CREATE TABLE `cc_coupon_costumes` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_coupon_history`
--

CREATE TABLE `cc_coupon_history` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_creditcard`
--

CREATE TABLE `cc_creditcard` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `cardholder_name` varchar(120) DEFAULT NULL,
  `credit_card_mask` varchar(150) NOT NULL,
  `card_type` varchar(50) DEFAULT NULL,
  `last_digits` varchar(150) NOT NULL DEFAULT '0000',
  `is_default` enum('0','1') NOT NULL DEFAULT '0',
  `payment_method_token` varchar(150) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `exp_month` int(11) NOT NULL,
  `exp_year` int(11) NOT NULL,
  `cvn_pin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_creditcard`
--

INSERT INTO `cc_creditcard` (`id`, `user_id`, `cardholder_name`, `credit_card_mask`, `card_type`, `last_digits`, `is_default`, `payment_method_token`, `created_on`, `updated_at`, `created_at`, `exp_month`, `exp_year`, `cvn_pin`) VALUES
(4, 88, 'jithender kumar', '555555******4444', 'MasterCard', '4444', '0', 'b6fhj4', '0000-00-00 00:00:00', NULL, '2017-06-06 09:58:15', 3, 2021, 123);

-- --------------------------------------------------------

--
-- Table structure for table `cc_customer_wishlist`
--

CREATE TABLE `cc_customer_wishlist` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_events`
--

CREATE TABLE `cc_events` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(20) NOT NULL,
  `event_url` varchar(63) NOT NULL,
  `user_img` varchar(127) NOT NULL,
  `from_date` date NOT NULL,
  `from_time` time(6) NOT NULL,
  `to_date` date NOT NULL,
  `to_time` time(6) NOT NULL,
  `event_desc` text NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `address_id` int(11) NOT NULL,
  `mentioned_user_id` bigint(20) NOT NULL,
  `created_by` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_events`
--

INSERT INTO `cc_events` (`event_id`, `event_name`, `event_url`, `user_img`, `from_date`, `from_time`, `to_date`, `to_time`, `event_desc`, `approved`, `created_at`, `address_id`, `mentioned_user_id`, `created_by`) VALUES
(1, 'Event 1', 'Event URL', 'ExWwzlXs9N.png', '2017-05-01', '01:20:00.000000', '2017-05-08', '08:40:00.000000', '<p>kjkldslfjlsdjkfll</p>', 1, '2017-05-31 05:37:02', 193, 0, 1),
(2, 'Event 1', 'Event URL', 'ExWwzlXs9N.png', '2017-05-01', '01:20:00.000000', '2017-05-08', '08:40:00.000000', '<p>kjkldslfjlsdjkfll</p>', 1, '2017-05-31 05:37:02', 193, 0, 1),
(3, 'Offer on costumes ', 'http://chrysalisqa.local.dotcomweavers.net/events', 'DnJvR0cXj6.png', '2017-05-31', '02:30:00.000000', '2017-06-02', '03:30:00.000000', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><div style="margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; text-align: left; float: right; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br class="Apple-interchange-newline"></div><p><br></p>', 1, '2017-05-31 06:19:48', 195, 0, 1),
(5, 'Testing', 'http://chrysalisqa.local.dotcomweavers.net/category/womens/tv-m', 'KvwippmRgD.png', '2017-05-26', '06:30:00.000000', '2017-05-27', '18:25:00.000000', '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span><br></p>', 1, '2017-05-31 09:31:46', 197, 0, 1),
(7, 'Costumes Expo', 'Chrysalis.com ', 'cHFAyFLS7u.png', '2017-06-01', '18:00:00.000000', '2017-06-14', '06:50:00.000000', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><br>', 0, '2017-06-01 02:15:42', 198, 0, 1),
(8, 'Costumes Expo', 'Chrysalis.com ', 'cHFAyFLS7u.png', '2017-06-01', '18:00:00.000000', '2017-06-14', '06:50:00.000000', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><br>', 0, '2017-06-01 02:15:42', 198, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cc_event_tags`
--

CREATE TABLE `cc_event_tags` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tags` varchar(512) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_language`
--

CREATE TABLE `cc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_language`
--

INSERT INTO `cc_language` (`language_id`, `name`, `code`, `locale`, `image`, `sort_order`, `status`) VALUES
(1, 'English', 'en', 'en-in', '', 0, 1),
(2, 'Spanish', 'es', 'es-mx', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cc_menu`
--

CREATE TABLE `cc_menu` (
  `menu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_link` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_menu`
--

INSERT INTO `cc_menu` (`menu_id`, `name`, `url_link`, `sort_order`, `status`, `parent_id`) VALUES
(1, 'Men''s', '', 0, 1, 0),
(2, 'Women''s', '', 1, 1, 0),
(3, 'Kid''s', '', 2, 1, 0),
(4, 'Pets', '', 3, 1, 0),
(5, 'Holiday', '', 4, 1, 0),
(6, 'Cosplay', '', 5, 1, 0),
(7, 'Unique Fashion', '', 6, 1, 0),
(8, 'Film & Theatre', '', 7, 1, 0),
(9, 'Animals & Insects', '', 0, 1, 1),
(10, 'Tv & Movie', '', 1, 1, 1),
(11, 'Superheroes & Villains', '', 2, 1, 1),
(12, 'Horror', '', 3, 1, 1),
(13, 'Storybook & Fairytale', '', 4, 1, 1),
(14, 'Historical Figures', '', 5, 1, 1),
(15, 'Groups & Couples', '', 6, 1, 1),
(16, 'Miscellaneous', '', 7, 1, 1),
(17, 'Animals & Insects', '', 0, 1, 2),
(18, 'Tv & Movie', '', 1, 1, 2),
(19, 'Superheroes & Villains', '', 2, 1, 2),
(20, 'Horror', '', 3, 1, 2),
(21, 'Storybook & Fairytale', '', 4, 1, 2),
(22, 'Historical Figures', '', 5, 1, 2),
(23, 'Groups & Couples', '', 6, 1, 2),
(24, 'Miscellaneous', '', 7, 1, 2),
(25, 'Animals & Insects', '', 0, 1, 3),
(26, 'Tv & Movie', '', 1, 1, 3),
(27, 'Superheroes & Villains', '', 2, 1, 3),
(28, 'Horror', '', 3, 1, 3),
(29, 'Storybook & Fairytale', '', 4, 1, 3),
(30, 'Historical Figures', '', 5, 1, 3),
(31, 'Groups & Couples', '', 6, 1, 3),
(32, 'Miscellaneous', '', 7, 1, 3),
(33, 'Anime/Manga', '', 0, 1, 6),
(34, 'Comic/Superheroes', '', 1, 1, 6),
(35, 'Furries', '', 2, 1, 6),
(36, 'Film & TV', '', 3, 1, 6),
(37, 'Mecha', '', 4, 1, 6),
(38, 'Sci-Fi', '', 5, 1, 6),
(39, 'Video Games', '', 6, 1, 6),
(40, 'Other', '', 7, 1, 6),
(41, 'Cyberpunk', '', 0, 1, 7),
(42, 'Dystopian', '', 1, 1, 7),
(43, 'Goth', '', 2, 1, 7),
(44, 'Steampunk', '', 3, 1, 7),
(45, 'Streetwear', '', 4, 1, 7),
(46, 'Lolita', '', 5, 1, 7),
(47, 'Mori Kei', '', 6, 1, 7),
(48, 'Fairy Kei', '', 7, 1, 7),
(49, 'Visual Kei', '', 8, 1, 7),
(50, 'Other', '', 9, 1, 7),
(51, 'Circus', '', 0, 1, 8),
(52, 'Historical Reenactments', '', 1, 1, 8),
(53, 'LARP', '', 2, 1, 8),
(54, 'Masquerade', '', 3, 1, 8),
(55, 'Medieval/Renaissance Fairs', '', 4, 1, 8),
(56, 'Theatre', '', 5, 1, 8),
(57, 'Music Videos', '', 6, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `cc_menu_description`
--

CREATE TABLE `cc_menu_description` (
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_messages`
--

CREATE TABLE `cc_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_from_sender` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_from_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `user_name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conversation_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cc_messages`
--

INSERT INTO `cc_messages` (`id`, `message`, `is_seen`, `deleted_from_sender`, `deleted_from_receiver`, `user_id`, `user_name`, `conversation_id`, `created_at`, `updated_at`) VALUES
(1, 'hi\r\n', 0, 0, 0, 34, NULL, 40, '2017-05-23 01:52:21', '2017-05-23 01:52:21'),
(2, 'hi', 0, 0, 0, 34, NULL, 41, '2017-05-24 21:11:00', '2017-05-24 21:11:00'),
(3, 'Hi', 0, 0, 0, 40, NULL, 42, NULL, NULL),
(4, 'Hi', 0, 0, 0, 40, NULL, 43, NULL, NULL),
(6, 'Hi', 0, 0, 0, 42, NULL, 45, NULL, NULL),
(7, 'Hi', 0, 0, 0, 42, NULL, 46, NULL, NULL),
(8, 'Hi', 0, 0, 0, 41, NULL, 48, NULL, NULL),
(9, 'Hi', 0, 0, 0, 41, NULL, 51, NULL, NULL),
(10, 'Hi', 0, 0, 0, 41, NULL, 52, NULL, NULL),
(11, 'Hi', 0, 0, 0, 41, NULL, 53, NULL, NULL),
(12, 'Hi', 0, 0, 0, 41, NULL, 54, NULL, NULL),
(23, 'hi', 0, 0, 0, 44, NULL, 60, '2017-05-29 21:08:13', '2017-05-29 21:08:13'),
(26, '5353', 0, 0, 0, 44, NULL, 60, '2017-05-29 21:17:16', '2017-05-29 21:17:16'),
(27, '66', 0, 0, 0, 44, NULL, 60, '2017-05-29 21:17:20', '2017-05-29 21:17:20'),
(28, 'Hi', 0, 0, 0, 44, NULL, 61, '2017-05-29 21:21:21', NULL),
(29, 'sdffsd', 0, 0, 0, 44, NULL, 60, '2017-05-29 21:23:04', '2017-05-29 21:23:04'),
(42, '', 0, 0, 0, 1, NULL, 61, '2017-05-30 04:06:07', NULL),
(43, '', 0, 0, 0, 1, NULL, 61, '2017-05-30 04:06:14', NULL),
(44, 'Hi', 0, 0, 0, 43, NULL, 66, '2017-05-30 21:17:33', NULL),
(45, '5324', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:19:22', '2017-05-30 21:19:22'),
(46, 'Hii', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:24:54', '2017-05-30 21:24:54'),
(47, 'Bag charges ', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:25:28', '2017-05-30 21:25:28'),
(48, 'Processing fees', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:29:26', '2017-05-30 21:29:26'),
(49, 'charge', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:30:32', '2017-05-30 21:30:32'),
(50, '5', 0, 0, 0, 43, NULL, 67, '2017-05-30 21:35:49', '2017-05-30 21:35:49'),
(51, 'Hi', 0, 0, 0, 1, NULL, 3, '2017-05-30 21:46:35', '2017-05-30 21:46:35'),
(52, 'tetsing', 0, 0, 0, 1, NULL, 66, '2017-05-31 04:30:44', NULL),
(53, 'Hi', 0, 0, 0, 1, NULL, 65, '2017-05-31 19:54:35', NULL),
(54, 'Hi', 0, 0, 0, 1, NULL, 65, '2017-05-31 19:54:38', NULL),
(55, 'Order error ', 0, 0, 0, 43, NULL, 68, NULL, NULL),
(56, 'Shipping is not included', 0, 0, 0, 43, NULL, 69, NULL, NULL),
(57, 'Hi', 0, 0, 0, 1, NULL, 70, '2017-05-31 22:04:54', NULL),
(59, 'Hi', 0, 0, 0, 42, NULL, 72, NULL, NULL),
(60, 'Hi', 0, 0, 0, 42, NULL, 73, NULL, NULL),
(61, 'Test messge', 0, 0, 0, 34, NULL, 41, '2017-06-01 00:07:33', '2017-06-01 00:07:33'),
(62, 'Test again againg', 0, 0, 0, 34, NULL, 41, '2017-06-01 00:07:45', '2017-06-01 00:07:45'),
(63, 'Hi', 0, 0, 0, 43, NULL, 74, '2017-06-01 00:34:02', NULL),
(64, 'Testing\n', 0, 0, 0, 1, NULL, 69, '2017-06-01 01:07:05', NULL),
(65, 'Order is not delivered', 0, 0, 0, 43, NULL, 75, NULL, NULL),
(66, 'Order status', 0, 0, 0, 1, NULL, 69, '2017-06-01 01:16:41', NULL),
(67, '', 0, 0, 0, 1, NULL, 69, '2017-06-01 01:16:59', NULL),
(68, 'Order is disputed', 0, 0, 0, 43, NULL, 76, NULL, NULL),
(69, 'Order is not shipped ', 0, 0, 0, 43, NULL, 77, NULL, NULL),
(70, 'Ticket is closed', 0, 0, 0, 65, NULL, 77, '2017-06-01 01:50:58', NULL),
(71, 'Order is not processed', 0, 0, 0, 43, NULL, 78, NULL, NULL),
(72, 'hi', 0, 0, 0, 1, NULL, 78, '2017-06-01 01:55:14', NULL),
(73, 'hi', 0, 0, 0, 1, NULL, 78, '2017-06-01 01:55:18', NULL),
(74, '', 0, 0, 0, 43, NULL, 79, NULL, NULL),
(75, '', 0, 0, 0, 43, NULL, 80, NULL, NULL),
(76, '', 0, 0, 0, 43, NULL, 81, NULL, NULL),
(77, 'Order is not placing ', 0, 0, 0, 43, NULL, 82, NULL, NULL),
(78, '', 0, 0, 0, 43, NULL, 83, NULL, NULL),
(79, '', 0, 0, 0, 1, NULL, 84, NULL, NULL),
(80, 'hi', 0, 0, 0, 1, NULL, 83, '2017-06-01 03:10:00', NULL),
(81, 'Order information wheteher shiped ', 0, 0, 0, 66, NULL, 85, NULL, NULL),
(82, 'hii', 0, 0, 0, 1, NULL, 85, '2017-06-01 03:13:32', NULL),
(83, 'xzczxc', 0, 0, 0, 1, NULL, 86, NULL, NULL),
(84, 'Information about the order', 0, 0, 0, 1, NULL, 87, NULL, NULL),
(85, 'Shipping is not processed', 0, 0, 0, 1, NULL, 88, NULL, NULL),
(86, 'About the order information ', 0, 0, 0, 1, NULL, 89, NULL, NULL),
(87, 'Hi', 0, 0, 0, 44, NULL, 90, NULL, NULL),
(88, 'Hi', 0, 0, 0, 44, NULL, 91, NULL, NULL),
(89, 'Hi', 0, 0, 0, 42, NULL, 92, NULL, NULL),
(90, 'Hi', 0, 0, 0, 42, NULL, 93, NULL, NULL),
(91, 'Hi', 0, 0, 0, 42, NULL, 94, NULL, NULL),
(92, 'Hi', 0, 0, 0, 42, NULL, 95, NULL, NULL),
(93, 'Hi', 0, 0, 0, 42, NULL, 96, NULL, NULL),
(94, 'Hi', 0, 0, 0, 42, NULL, 97, NULL, NULL),
(95, 'Hi', 0, 0, 0, 42, NULL, 98, NULL, NULL),
(96, 'Hi', 0, 0, 0, 42, NULL, 99, NULL, NULL),
(97, 'Testing the order', 0, 0, 0, 43, NULL, 100, NULL, NULL),
(98, 'Hi', 0, 0, 0, 68, NULL, 101, NULL, NULL),
(99, 'Hi', 0, 0, 0, 68, NULL, 102, NULL, NULL),
(100, 'Hi', 0, 0, 0, 68, NULL, 103, NULL, NULL),
(101, 'Hi', 0, 0, 0, 68, NULL, 104, NULL, NULL),
(102, 'Hi', 0, 0, 0, 68, NULL, 105, NULL, NULL),
(103, 'Hi', 0, 0, 0, 68, NULL, 106, NULL, NULL),
(104, 'Hi', 0, 0, 0, 68, NULL, 107, NULL, NULL),
(105, 'Hi', 0, 0, 0, 68, NULL, 108, NULL, NULL),
(106, 'Hi', 0, 0, 0, 68, NULL, 109, NULL, NULL),
(107, 'Hi', 0, 0, 0, 68, NULL, 110, NULL, NULL),
(108, 'Hi', 0, 0, 0, 68, NULL, 111, NULL, NULL),
(109, 'Hi', 0, 0, 0, 68, NULL, 112, NULL, NULL),
(110, 'Hi', 0, 0, 0, 68, NULL, 113, NULL, NULL),
(111, 'Hi', 0, 0, 0, 68, NULL, 114, NULL, NULL),
(112, 'Hi', 0, 0, 0, 68, NULL, 115, NULL, NULL),
(113, 'Hi', 0, 0, 0, 68, NULL, 116, NULL, NULL),
(114, 'Hi', 0, 0, 0, 85, NULL, 117, NULL, NULL),
(115, 'Hi', 0, 0, 0, 85, NULL, 118, NULL, NULL),
(116, 'Hi', 0, 0, 0, 85, NULL, 119, NULL, NULL),
(117, 'Hi', 0, 0, 0, 85, NULL, 120, NULL, NULL),
(118, 'Hi', 0, 0, 0, 85, NULL, 121, NULL, NULL),
(119, 'Hi', 0, 0, 0, 85, NULL, 122, NULL, NULL),
(120, 'Hi', 0, 0, 0, 85, NULL, 123, NULL, NULL),
(121, 'Hi', 0, 0, 0, 85, NULL, 124, NULL, NULL),
(122, 'Hi', 0, 0, 0, 85, NULL, 125, NULL, NULL),
(123, 'Hi', 0, 0, 0, 85, NULL, 126, NULL, NULL),
(124, 'Hi', 0, 0, 0, 85, NULL, 127, NULL, NULL),
(125, 'Hi', 0, 0, 0, 85, NULL, 128, NULL, NULL),
(126, 'Hi', 0, 0, 0, 85, NULL, 129, NULL, NULL),
(127, 'Hi', 0, 0, 0, 85, NULL, 130, NULL, NULL),
(128, 'Hi', 0, 0, 0, 85, NULL, 131, NULL, NULL),
(129, 'Hi', 0, 0, 0, 85, NULL, 132, NULL, NULL),
(130, 'Hi', 0, 0, 0, 85, NULL, 133, NULL, NULL),
(131, 'Hi', 0, 0, 0, 85, NULL, 134, NULL, NULL),
(132, 'Hi', 0, 0, 0, 85, NULL, 135, NULL, NULL),
(133, 'Hi', 0, 0, 0, 85, NULL, 136, NULL, NULL),
(134, 'Hi', 0, 0, 0, 88, 'jithender kumar', 138, NULL, NULL),
(135, 'Hi', 0, 0, 0, 88, 'jithender kumar', 139, NULL, NULL),
(136, 'Hi', 0, 0, 0, 88, 'jithender kumar', 140, NULL, NULL),
(137, 'Hi', 0, 0, 0, 88, 'jithender kumar', 141, NULL, NULL),
(138, 'Hi', 0, 0, 0, 88, 'jithender kumar', 142, NULL, NULL),
(139, 'Hi', 0, 0, 0, 88, 'jithender kumar', 143, NULL, NULL),
(140, 'Hi', 0, 0, 0, 88, 'jithender kumar', 144, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cc_order`
--

CREATE TABLE `cc_order` (
  `order_id` int(11) NOT NULL,
  `buyer_id` bigint(20) NOT NULL DEFAULT '0',
  `seller_id` int(20) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `phone_no` varchar(31) NOT NULL,
  `pay_firstname` varchar(31) NOT NULL,
  `pay_lastname` varchar(31) NOT NULL,
  `pay_address_1` varchar(127) NOT NULL,
  `pay_address_2` varchar(127) NOT NULL,
  `pay_city` varchar(127) NOT NULL,
  `pay_state` varchar(100) NOT NULL,
  `pay_zipcode` varchar(31) NOT NULL,
  `pay_country` varchar(127) NOT NULL,
  `payment_method` varchar(127) NOT NULL,
  `payment_code` varchar(127) NOT NULL,
  `shipping_firstname` varchar(31) NOT NULL,
  `shipping_lastname` varchar(31) NOT NULL,
  `shipping_address_1` varchar(127) NOT NULL,
  `shipping_address_2` varchar(127) NOT NULL,
  `shipping_city` varchar(127) NOT NULL,
  `shipping_state` varchar(100) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(10,4) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_order`
--

INSERT INTO `cc_order` (`order_id`, `buyer_id`, `seller_id`, `firstname`, `lastname`, `email`, `phone_no`, `pay_firstname`, `pay_lastname`, `pay_address_1`, `pay_address_2`, `pay_city`, `pay_state`, `pay_zipcode`, `pay_country`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_state`, `shipping_postcode`, `shipping_country`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `cc_id`, `created_at`, `modified_at`) VALUES
(37, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', 'jithender', 'kumar', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', 'jithender', 'kumar', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'express', '', '', '0.00', 0, 0, '0.0000', 4, '2017-06-06 10:11:53', '0000-00-00 00:00:00'),
(38, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', 'jithender', 'kumar', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', 'jithender', 'kumar', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'express', '', '', '249.40', 1, 0, '0.0000', 4, '2017-06-06 10:19:26', '0000-00-00 00:00:00'),
(39, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'priority', '', '', '164.75', 1, 0, '0.0000', 4, '2017-06-06 10:29:02', '0000-00-00 00:00:00'),
(40, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'priority', '', '', '44.70', 1, 0, '0.0000', 4, '2017-06-06 10:38:17', '0000-00-00 00:00:00'),
(41, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'priority', '', '', '164.75', 1, 0, '0.0000', 4, '2017-06-06 10:41:45', '0000-00-00 00:00:00'),
(42, 88, 86, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', 'sample', 'sample1', '106 Bennett Cv', '106 Bennett Cv', 'Hutto', 'Texas', '78634', 'United States', 'express', '', '', '517.10', 2, 0, '0.0000', 4, '2017-06-06 10:41:47', '0000-00-00 00:00:00'),
(43, 88, 86, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'express', '', '', '103.10', 1, 0, '0.0000', 4, '2017-06-06 11:32:22', '0000-00-00 00:00:00'),
(44, 88, 87, 'jithender', 'kumar', 'jithender@dotcomweavers.com', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', '', '39201', 'United States', '', '', '', '', '4794 Reppert Coal Road', '4794 Reppert Coal Road', 'Jackson', 'Mississippi', '39201', 'United States', 'priority', '', '', '164.75', 1, 0, '0.0000', 4, '2017-06-06 11:32:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_charity`
--

CREATE TABLE `cc_order_charity` (
  `id` int(11) NOT NULL,
  `order_id` varchar(150) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `charity_id` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_order_charity`
--

INSERT INTO `cc_order_charity` (`id`, `order_id`, `user_id`, `charity_id`, `amount`, `created_at`) VALUES
(7, '39', 88, 20, '1', '2017-06-06 10:29:12'),
(8, '40', 88, 20, '1', '2017-06-06 10:38:25'),
(9, '41,42', 88, 20, '1', '2017-06-06 10:41:58'),
(10, '43,44', 88, 21, '1', '2017-06-06 11:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_disputes`
--

CREATE TABLE `cc_order_disputes` (
  `dispute_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` enum('admin','user') NOT NULL DEFAULT 'user',
  `dispute_msg` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_history`
--

CREATE TABLE `cc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_items`
--

CREATE TABLE `cc_order_items` (
  `order_item_id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `sku` varchar(127) DEFAULT NULL,
  `costume_name` varchar(255) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `price` float NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_order_items`
--

INSERT INTO `cc_order_items` (`order_item_id`, `costume_id`, `sku`, `costume_name`, `qty`, `price`, `order_id`) VALUES
(71, 110, 'CS000004', 'jithendertest2-1', 1, 100, 38),
(72, 111, 'CS000005', 'jithendertest2-2', 1, 10, 38),
(73, 110, 'CS000004', 'jithendertest2-1', 1, 100, 39),
(74, 111, 'CS000005', 'jithendertest2-2', 1, 10, 39),
(75, 111, 'CS000005', 'jithendertest2-2', 1, 10, 40),
(76, 110, 'CS000004', 'jithendertest2-1', 1, 100, 41),
(77, 111, 'CS000005', 'jithendertest2-2', 1, 10, 41),
(78, 108, 'CS000002', 'jithendertest1-2', 2, 212, 42),
(79, 109, 'CS000003', 'jithendertest1-3', 1, 10, 43),
(80, 110, 'CS000004', 'jithendertest2-1', 1, 100, 44),
(81, 111, 'CS000005', 'jithendertest2-2', 1, 10, 44);

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_ship_track`
--

CREATE TABLE `cc_order_ship_track` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `weight` decimal(15,0) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `carrier_type` enum('fedex','usps') NOT NULL,
  `shipment_status` varchar(127) NOT NULL,
  `carrier_code` varchar(127) NOT NULL,
  `carrier_desc` varchar(255) NOT NULL,
  `track_no` varchar(127) DEFAULT NULL,
  `is_notify` enum('0','1') NOT NULL DEFAULT '1',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_order_ship_track`
--

INSERT INTO `cc_order_ship_track` (`id`, `order_id`, `weight`, `qty`, `user_id`, `carrier_type`, `shipment_status`, `carrier_code`, `carrier_desc`, `track_no`, `is_notify`, `amount`, `created_at`) VALUES
(1, 42, '12', 0, 88, 'usps', '', 'PRIORITY', '', '420786349405501699320788122096', '1', '7.50', '2017-06-06 11:05:24');

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_status`
--

CREATE TABLE `cc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_order_status`
--

INSERT INTO `cc_order_status` (`order_status_id`, `order_id`, `status_id`, `comment`, `created_at`) VALUES
(37, 38, 1, '', '0000-00-00 00:00:00'),
(38, 39, 1, '', '0000-00-00 00:00:00'),
(39, 40, 1, '', '0000-00-00 00:00:00'),
(40, 41, 1, '', '0000-00-00 00:00:00'),
(41, 42, 1, '', '0000-00-00 00:00:00'),
(42, 42, 2, '', '2017-06-06 11:05:24'),
(43, 43, 1, '', '0000-00-00 00:00:00'),
(44, 44, 1, '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_order_total`
--

CREATE TABLE `cc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_order_total`
--

INSERT INTO `cc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(39, 38, 'add', 'Subtotal', '110.0000', 0),
(40, 38, 'add', 'Shipping', '139.4000', 1),
(41, 39, 'add', 'Subtotal', '110.0000', 0),
(42, 39, 'add', 'Shipping', '54.7500', 1),
(43, 40, 'add', 'Subtotal', '10.0000', 0),
(44, 40, 'add', 'Shipping', '34.7000', 1),
(45, 41, 'add', 'Subtotal', '110.0000', 0),
(46, 41, 'add', 'Shipping', '54.7500', 1),
(47, 42, 'add', 'Subtotal', '424.0000', 0),
(48, 42, 'add', 'Shipping', '93.1000', 1),
(49, 43, 'add', 'Subtotal', '10.0000', 0),
(50, 43, 'add', 'Shipping', '93.1000', 1),
(51, 44, 'add', 'Subtotal', '110.0000', 0),
(52, 44, 'add', 'Shipping', '54.7500', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cc_permissions`
--

CREATE TABLE `cc_permissions` (
  `permission_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_permissions`
--

INSERT INTO `cc_permissions` (`permission_id`, `code`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'sample', 'sample', 'sample', 'active', '2016-06-22 14:43:47', '2016-06-24 05:24:43'),
(3, 'sample2', 'sample2', 'sample2', 'active', '2016-06-22 14:44:10', '2016-06-23 14:55:56'),
(4, 'sample3', 'sample3', 'sample3', 'active', '2016-06-23 11:42:50', '2016-06-23 14:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `cc_press`
--

CREATE TABLE `cc_press` (
  `press_id` int(11) NOT NULL,
  `press_title` varchar(127) NOT NULL,
  `source` varchar(127) NOT NULL,
  `press_desc` text NOT NULL,
  `user_img` varchar(127) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_press`
--

INSERT INTO `cc_press` (`press_id`, `press_title`, `source`, `press_desc`, `user_img`, `status`, `created_at`, `updated_at`) VALUES
(13, 'testing update 2', '', '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', '', 0, '2017-06-01 02:04:21', '0000-00-00 00:00:00'),
(16, 'CHRYSALSI-REVOLUTIONARY', '', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><div style="margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; text-align: left; float: right; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br class="Apple-interchange-newline"></div><p><br></p>', '', 0, '2017-05-31 08:00:34', '0000-00-00 00:00:00'),
(17, 'HOW IT WORKS', '', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><div style="margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; text-align: left; float: right; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br class="Apple-interchange-newline"></div><p><br></p>', '', 0, '2017-05-31 08:13:07', '0000-00-00 00:00:00'),
(19, 'Costumes ', 'Costume expo ', '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', 'VjgttsEiSv.png', 0, '2017-06-02 00:51:02', '0000-00-00 00:00:00'),
(20, 'Costumes', 'Costumes for sale ', '<p><br></p><div style="margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; text-align: left; float: left; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;"><strong style="margin: 0px; padding: 0px;">Lorem Ipsum</strong><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><p><br></p><div style="margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; text-align: left; float: right; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><h2 style="margin: 0px 0px 10px; padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; text-align: left;">Why do we use it?</h2><p style="margin: 0px 0px 15px; padding: 0px; text-align: justify;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><p><br class="Apple-interchange-newline"><br></p>', 'oRv7oLNHJc.png', 0, '2017-06-02 01:34:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_press_categories`
--

CREATE TABLE `cc_press_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_press_categories`
--

INSERT INTO `cc_press_categories` (`id`, `cat_name`) VALUES
(1, 'Category 1'),
(2, 'Category 2'),
(3, 'Category 3'),
(4, 'Category 4');

-- --------------------------------------------------------

--
-- Table structure for table `cc_press_cat_link`
--

CREATE TABLE `cc_press_cat_link` (
  `id` int(11) NOT NULL,
  `press_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_press_cat_link`
--

INSERT INTO `cc_press_cat_link` (`id`, `press_id`, `cat_id`) VALUES
(20, 14, 1),
(21, 14, 4),
(27, 15, 1),
(37, 16, 1),
(38, 17, 2),
(40, 13, 1),
(41, 13, 2),
(42, 18, 1),
(43, 18, 2),
(44, 18, 3),
(45, 19, 1),
(46, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cc_promotion_coupon`
--

CREATE TABLE `cc_promotion_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` enum('percentage','flat','free') NOT NULL COMMENT 'Discount or Flat amount',
  `discount` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL DEFAULT '0',
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_promotion_coupon`
--

INSERT INTO `cc_promotion_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `created_at`) VALUES
(2, 'Admin', 'admin', 'free', '0.0000', '2017-06-02', '2017-06-30', 12, '', 0, '0000-00-00 00:00:00'),
(3, 'New costumes', 'new', 'percentage', '12.0000', '2017-06-04', '2017-06-30', 12, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_reported_costumes`
--

CREATE TABLE `cc_reported_costumes` (
  `id` int(11) NOT NULL,
  `costume_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phn_no` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_request_bags`
--

CREATE TABLE `cc_request_bags` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `ref_no` varchar(255) NOT NULL,
  `addres_id` int(11) NOT NULL,
  `is_payout` enum('0','1') NOT NULL DEFAULT '0',
  `is_return` enum('0','1') NOT NULL DEFAULT '0',
  `is_recycle` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('Requested','Sent','Received','Paid','Partial paid','Return','Return received','Shipped','Closed') NOT NULL DEFAULT 'Requested',
  `cus_name` varchar(255) NOT NULL,
  `cus_email` varchar(255) NOT NULL,
  `cus_phone` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_request_credits`
--

CREATE TABLE `cc_request_credits` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `type` enum('payout','return') NOT NULL DEFAULT 'payout',
  `credit` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_request_shippings`
--

CREATE TABLE `cc_request_shippings` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `type` enum('pick','drop','return') NOT NULL DEFAULT 'pick',
  `weight` float NOT NULL DEFAULT '0',
  `shipping_no` varchar(127) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_request_status`
--

CREATE TABLE `cc_request_status` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `status` enum('Requested Bag','Shipped','Paid out','Returned','Closed') NOT NULL DEFAULT 'Requested Bag',
  `weight` float NOT NULL DEFAULT '0',
  `shipping_no` varchar(127) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_roles`
--

CREATE TABLE `cc_roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `default_context` varchar(255) DEFAULT 'content',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_role_permissions`
--

CREATE TABLE `cc_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_states`
--

CREATE TABLE `cc_states` (
  `id` int(11) NOT NULL,
  `name` char(40) NOT NULL,
  `abbrev` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_states`
--

INSERT INTO `cc_states` (`id`, `name`, `abbrev`) VALUES
(1, 'Alaska', 'AK'),
(2, 'Alabama', 'AL'),
(3, 'American Samoa', 'AS'),
(4, 'Arizona', 'AZ'),
(5, 'Arkansas', 'AR'),
(6, 'California', 'CA'),
(7, 'Colorado', 'CO'),
(8, 'Connecticut', 'CT'),
(9, 'Delaware', 'DE'),
(10, 'District of Columbia', 'DC'),
(11, 'Florida', 'FL'),
(12, 'Georgia', 'GA'),
(13, 'Guam', 'GU'),
(14, 'Hawaii', 'HI'),
(15, 'Idaho', 'ID'),
(16, 'Illinois', 'IL'),
(17, 'Indiana', 'IN'),
(18, 'Iowa', 'IA'),
(19, 'Kansas', 'KS'),
(20, 'Kentucky', 'KY'),
(21, 'Louisiana', 'LA'),
(22, 'Maine', 'ME'),
(23, 'Marshall Islands', 'MH'),
(24, 'Maryland', 'MD'),
(25, 'Massachusetts', 'MA'),
(26, 'Michigan', 'MI'),
(27, 'Minnesota', 'MN'),
(28, 'Mississippi', 'MS'),
(29, 'Missouri', 'MO'),
(30, 'Montana', 'MT'),
(31, 'Nebraska', 'NE'),
(32, 'Nevada', 'NV'),
(33, 'New Hampshire', 'NH'),
(34, 'New Jersey', 'NJ'),
(35, 'New Mexico', 'NM'),
(36, 'New York', 'NY'),
(37, 'North Carolina', 'NC'),
(38, 'North Dakota', 'ND'),
(39, 'Northern Mariana Islands', 'MP'),
(40, 'Ohio', 'OH'),
(41, 'Oklahoma', 'OK'),
(42, 'Oregon', 'OR'),
(43, 'Palau', 'PW'),
(44, 'Pennsylvania', 'PA'),
(45, 'Puerto Rico', 'PR'),
(46, 'Rhode Island', 'RI'),
(47, 'South Carolina', 'SC'),
(48, 'South Dakota', 'SD'),
(49, 'Tennessee', 'TN'),
(50, 'Texas', 'TX'),
(51, 'Utah', 'UT'),
(52, 'Vermont', 'VT'),
(53, 'Virgin Islands', 'VI'),
(54, 'Virginia', 'VA'),
(55, 'Washington', 'WA'),
(56, 'West Virginia', 'WV'),
(57, 'Wisconsin', 'WI'),
(58, 'Wyoming', 'WY'),
(59, 'Armed Forces Africa', 'AE'),
(60, 'Armed Forces Canada', 'AE'),
(61, 'Armed Forces Europe', 'AE'),
(62, 'Armed Forces Middle East', 'AE'),
(63, 'Armed Forces Pacific', 'AP');

-- --------------------------------------------------------

--
-- Table structure for table `cc_status`
--

CREATE TABLE `cc_status` (
  `status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(31) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_status`
--

INSERT INTO `cc_status` (`status_id`, `language_id`, `name`) VALUES
(1, 1, 'Processing'),
(2, 1, 'Shipping'),
(3, 1, 'Shipped'),
(4, 1, 'Dispatched'),
(5, 1, 'Delivered'),
(6, 1, 'Returned');

-- --------------------------------------------------------

--
-- Table structure for table `cc_tickets`
--

CREATE TABLE `cc_tickets` (
  `id` int(11) NOT NULL,
  `ticket_id` varchar(200) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `ticket_type` varchar(200) NOT NULL,
  `ticket_message` text NOT NULL,
  `ticket_reason` text NOT NULL,
  `ticket_userid` int(11) NOT NULL,
  `ticket_createddate` datetime NOT NULL,
  `ticket_status` tinyint(4) NOT NULL DEFAULT '1',
  `ticket_updateddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `conversation_id` int(11) NOT NULL,
  `ticket_priority` int(11) NOT NULL,
  `ticket_assigned_to` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_tickets`
--

INSERT INTO `cc_tickets` (`id`, `ticket_id`, `order_id`, `ticket_type`, `ticket_message`, `ticket_reason`, `ticket_userid`, `ticket_createddate`, `ticket_status`, `ticket_updateddate`, `conversation_id`, `ticket_priority`, `ticket_assigned_to`) VALUES
(34, 'CSSP000004', 'asdasd', 'Dispute', 'asdasd', 'asdasd', 37, '2017-05-31 07:24:13', 0, '2017-06-01 17:40:42', 30, 0, 0),
(35, 'CSSP000005', 'asdasd', 'Dispute', 'asdasd', 'asdasd', 37, '2017-05-31 07:24:49', 0, '2017-06-01 17:40:44', 31, 0, 0),
(36, 'CSSP000006', 'asdasd', 'Dispute', 'asdasddas', 'sadads', 37, '2017-05-31 07:25:41', 0, '2017-06-01 17:40:45', 32, 0, 0),
(37, 'CSSP000007', 'asd', 'Order', 'asdasd', 'asdas', 37, '2017-05-31 07:26:18', 1, '2017-05-31 16:56:19', 33, 0, 0),
(38, 'CSSP000008', 'asdasdas', 'Dispute', 'asdasdddasd', 'asdasd', 37, '2017-05-31 07:28:08', 2, '2017-06-01 08:51:11', 34, 1, 66),
(39, 'CSSP000009', 'asdasd', 'Order', 'asdasdas', 'asdasdd', 37, '2017-05-31 07:28:43', 1, '2017-06-01 19:19:22', 35, 0, 0),
(58, 'CSSP0000010', '1000052', 'Order', 'Information about the order', 'Information about the order', 1, '2017-06-01 09:09:47', 0, '2017-06-02 03:38:11', 87, 2, 65),
(59, 'CSSP0000011', '1000085', 'shipping', 'Shipping is not processed', 'Status about the order', 1, '2017-06-01 09:11:39', 1, '2017-06-02 13:00:16', 88, 1, 65),
(61, 'CSSP0000012', '10000073', 'Order', 'Testing the order', 'Order is not placing ', 43, '2017-06-02 03:24:26', 1, '2017-06-02 13:30:53', 100, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cc_transactions`
--

CREATE TABLE `cc_transactions` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `type` enum('charge','refund') NOT NULL DEFAULT 'charge',
  `amount` float(15,2) NOT NULL DEFAULT '0.00',
  `api_transaction_no` varchar(127) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_transactions`
--

INSERT INTO `cc_transactions` (`id`, `user_id`, `order_id`, `type`, `amount`, `api_transaction_no`, `cc_id`, `status`, `created_at`, `updated_at`) VALUES
(21, 88, 38, 'charge', 249.40, 'h045mrdw', 4, 'submitted_for_settlement', '2017-06-06 10:19:28', '2017-06-06 10:19:28'),
(22, 88, 39, 'charge', 164.75, '8rmr438m', 0, 'submitted_for_settlement', '2017-06-06 10:29:05', '2017-06-06 10:29:05'),
(23, 88, 40, 'charge', 44.70, '8wb9m5kb', 0, 'submitted_for_settlement', '2017-06-06 10:38:19', '2017-06-06 10:38:19'),
(24, 88, 41, 'charge', 164.75, '5dk1e4k5', 0, 'submitted_for_settlement', '2017-06-06 10:41:47', '2017-06-06 10:41:47'),
(25, 88, 42, 'charge', 517.10, '06pt8vzp', 0, 'submitted_for_settlement', '2017-06-06 10:41:49', '2017-06-06 10:41:49'),
(26, 88, 43, 'charge', 103.10, 'ezjjyqrf', 0, 'submitted_for_settlement', '2017-06-06 11:32:24', '2017-06-06 11:32:24'),
(27, 88, 44, 'charge', 164.75, 'fkvtvenr', 0, 'submitted_for_settlement', '2017-06-06 11:32:26', '2017-06-06 11:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `cc_url_rewrites`
--

CREATE TABLE `cc_url_rewrites` (
  `id` int(11) NOT NULL,
  `type` enum('product','category') NOT NULL,
  `url_offset` int(11) NOT NULL,
  `url_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_url_rewrites`
--

INSERT INTO `cc_url_rewrites` (`id`, `type`, `url_offset`, `url_key`) VALUES
(10, 'category', 72, '/mens/alladin'),
(11, 'category', 73, '/ttghh'),
(12, 'category', 74, '/pets'),
(14, 'category', 76, '/cosplay'),
(15, 'category', 77, '/unique-fashion'),
(16, 'category', 78, '/film-theatre'),
(19, 'category', 81, '/mens/superheroes-villains'),
(20, 'category', 82, '/mens/horror'),
(21, 'category', 83, '/mens/storybook-fairytale'),
(22, 'category', 84, '/mens/historical-figures'),
(23, 'category', 85, '/mens/groups-couples'),
(24, 'category', 86, '/mens/miscellaneous'),
(26, 'category', 88, '/womens/tv-movie'),
(27, 'category', 89, '/womens/superheroes-villains'),
(28, 'category', 90, '/womens/horror'),
(29, 'category', 91, '/womens/storybook-fairytale'),
(30, 'category', 92, '/womens/historical-figures'),
(31, 'category', 93, '/womens/groups-couples'),
(32, 'category', 94, '/miscellaneous'),
(33, 'category', 95, '/womens/miscellaneous'),
(34, 'category', 96, '/womens/miscellaneous'),
(35, 'category', 97, '/cosplay/anime-manga'),
(37, 'category', 99, '/cosplay/furries'),
(38, 'category', 100, '/cosplay/film-tv'),
(39, 'category', 101, '/cosplay/mecha'),
(40, 'category', 102, '/cosplay/sci-fi'),
(41, 'category', 103, '/cosplay/video-games'),
(52, 'category', 114, '/cosplay/other'),
(54, 'category', 105, '/unique-fashion/cyberpunk'),
(55, 'category', 106, '/unique-fashion/dystopian'),
(56, 'category', 107, '/unique-fashion/goth'),
(57, 'category', 108, '/unique-fashion/steampunk'),
(58, 'category', 109, '/unique-fashion/streetwear'),
(59, 'category', 110, '/unique-fashion/lolita'),
(60, 'category', 111, '/unique-fashion/mori-kei'),
(61, 'category', 112, '/unique-fashion/fairy-kei'),
(62, 'category', 113, '/unique-fashion/visual-kei'),
(63, 'category', 104, '/unique-fashion/others'),
(65, 'category', 115, '/unique-fashion/circus'),
(66, 'category', 116, '/unique-fashion/cyberpunk'),
(67, 'category', 117, '/unique-fashion/dystopian'),
(68, 'category', 118, '/unique-fashion/goth'),
(69, 'category', 119, '/unique-fashion/steampunk'),
(71, 'category', 121, '/unique-fashion/lolita'),
(72, 'category', 122, '/unique-fashion/mori-kei'),
(73, 'category', 123, '/unique-fashion/fairy-kei'),
(74, 'category', 124, '/unique-fashion/visual-kei'),
(75, 'category', 125, '/unique-fashion/other'),
(76, 'category', 126, '/film-theatre/circus'),
(77, 'category', 127, '/film-theatre/historical-reenactments'),
(78, 'category', 128, '/film-theatre/larp'),
(79, 'category', 129, '/film-theatre/masquerade'),
(80, 'category', 130, '/film-theatre/medieval-renaissance-fairs'),
(81, 'category', 131, '/film-theatre/theatre'),
(82, 'category', 132, '/film-theatre/music-videos'),
(88, 'category', 133, '/mens/fdgxfvb'),
(89, 'category', 134, '/mens/sczc'),
(90, 'category', 135, '/mens/ghf'),
(91, 'category', 120, '/unique-fashion/streetwear'),
(94, 'category', 136, '/toys'),
(95, 'product', 49, '/mens/animals-insects/pink-costume-for-pet'),
(96, 'product', 50, '/mens/horror/red-costume'),
(97, 'product', 51, '/womens/miscellaneous/pink-costume'),
(98, 'category', 137, '/film-theatre/vehicles'),
(99, 'product', 52, '/womens/tv-movie/barbie'),
(100, 'category', 138, '/womens/test'),
(101, 'product', 53, '/womens/groups-couples/123'),
(103, 'category', 140, '/mens/kensel'),
(104, 'product', 54, '/mens/miscellaneous/ddd'),
(105, 'product', 55, '/mens/tv-movie/new-coustume'),
(106, 'product', 56, '/pets/hallocks/farvik'),
(107, 'product', 57, '/unique-fashion/steampunk/senneri'),
(108, 'product', 58, '/unique-fashion/goth/johnny-costume'),
(109, 'category', 141, '/unique-fashion/goth'),
(110, 'category', 142, '/unique-fashion/goth'),
(111, 'product', 59, '/film-theatre/vehicles/test-costume'),
(112, 'product', 60, '/cosplay/comic-superhero/kids-costume'),
(113, 'product', 61, '/womens/horror/white-costume'),
(114, 'product', 62, '/film-theatre/vehicles/holiday-costume'),
(115, 'category', 143, '/unique-fashion'),
(116, 'category', 144, '/unique-fashion/fashion-costumes'),
(117, 'category', 98, '/cosplay/comic-superhero'),
(118, 'product', 63, '/film-theatre/vehicles/purple-costume'),
(119, 'product', 64, '/unique-fashion/fashion-costumes/test'),
(120, 'product', 65, '/pets/hallocks/white-costume'),
(121, 'product', 66, '/mens/animals-insects/jkhhhjk'),
(122, 'product', 67, '/film-theatre/theatre/yellow-costume'),
(123, 'product', 68, '/cosplay/comic-superhero/barbie-costume'),
(124, 'product', 69, '/film-theatre/masquerade/sample'),
(125, 'product', 70, '/mens/animals-insects/test-costume'),
(126, 'product', 71, '/mens/storybook-fairytale/costume-for-mens'),
(129, 'category', 145, '/mens/kids-costumes'),
(130, 'product', 72, '/mens/animals-insects/test'),
(131, 'product', 73, '/mens/miscellaneous/t-shirt'),
(132, 'product', 74, '/mens/tv-movie/test'),
(133, 'product', 75, '/mens/animals-insects/kids-costume'),
(134, 'product', 76, '/mens/animals-insects/rgvergver'),
(135, 'product', 77, '/womens/animals-insects/kids-costume'),
(136, 'product', 78, '/mens/storybook-fairytale/chrysalis-costume'),
(137, 'product', 79, '/mens/horror/hallowen'),
(138, 'category', 75, '/holiday'),
(142, 'category', 79, '/mens/animals-insects'),
(143, 'product', 80, '/pets/hallocks/pets-costume'),
(144, 'product', 81, '/film-theatre/circus/circus-costumes'),
(146, 'category', 139, '/pets/hallocks'),
(147, 'product', 82, '/pets/hallocks/barbies-costume'),
(148, 'product', 83, '/pets/hallocks/stylish-costume'),
(149, 'product', 84, '/unique-fashion/fashion-costumes/orange-costume'),
(150, 'product', 85, '/mens/kids-costumes/dotcom-weavers'),
(151, 'category', 66, '/mens'),
(152, 'category', 67, '/womens'),
(153, 'category', 68, '/kids'),
(154, 'product', 86, '/mens/animals-insects/green-costume-for-mens'),
(155, 'product', 87, '/womens/tv-movie/costume-for-kids'),
(157, 'product', 88, '/film-theatre/circus/circus-costume'),
(158, 'category', 146, '/womens/new-costumes'),
(159, 'product', 89, '/womens/historical-figures/costume-1'),
(160, 'product', 90, '/mens/animals-insects/test-costume-vamsi'),
(164, 'category', 87, '/womens/animals-insects'),
(165, 'category', 80, '/mens/tv-movie'),
(166, 'product', 91, '/cosplay/chrysalis-costume-1'),
(167, 'product', 92, '/mens/test'),
(168, 'product', 93, '/film-theatre/circus-costumes'),
(169, 'product', 94, '/womens/test-costume'),
(170, 'product', 95, '/mens/costume-in-blue'),
(171, 'product', 96, '/mens/costume-sample'),
(172, 'product', 97, '/mens/animals-insects/costume-sample'),
(173, 'product', 98, '/mens/animals-insects/coastume-sample1'),
(174, 'product', 99, '/mens/animals-insects/sample'),
(175, 'product', 101, '/mens/animals-insects/admin-costume'),
(176, 'product', 106, '/mens/tv-movie/mens-medium-spiderman-costume-in-red'),
(177, 'product', 107, '/mens/animals-insects/jithender-test1-1'),
(178, 'product', 108, '/mens/horror/jithendertest1-2'),
(179, 'product', 109, '/mens/animals-insects/jithendertest1-3'),
(180, 'product', 110, '/mens/animals-insects/jithendertest2-1'),
(181, 'product', 111, '/mens/animals-insects/jithendertest2-2');

-- --------------------------------------------------------

--
-- Table structure for table `cc_users`
--

CREATE TABLE `cc_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `email` varchar(254) NOT NULL,
  `password` char(60) DEFAULT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(31) DEFAULT NULL,
  `last_name` varchar(31) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `user_img` varchar(127) DEFAULT NULL,
  `newsletter` enum('0','1') NOT NULL DEFAULT '0',
  `phone_number` varchar(255) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `api_customer_id` varchar(100) DEFAULT NULL,
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `remember_token` varchar(255) DEFAULT NULL,
  `vacation_status` int(11) NOT NULL DEFAULT '0',
  `vacation_to` date NOT NULL,
  `vacation_from` date NOT NULL,
  `is_free` enum('0','1') NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `paypal_email` varchar(100) NOT NULL,
  `credits` decimal(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_users`
--

INSERT INTO `cc_users` (`id`, `role_id`, `email`, `password`, `reset_hash`, `last_login`, `created_at`, `updated_at`, `deleted`, `first_name`, `last_name`, `display_name`, `user_img`, `newsletter`, `phone_number`, `active`, `api_customer_id`, `activate_hash`, `remember_token`, `vacation_status`, `vacation_to`, `vacation_from`, `is_free`, `title`, `paypal_email`, `credits`) VALUES
(1, 1, 'admin@dotcomweavers.com', '$2y$10$eEoMkH42eTNF0eDMEKEal.GLpAtEIQXq8NqANgH8jiTDV3E9pBcDq', 'b65d4882d016b7e3a8ea9cb4165c7237', '0000-00-00 00:00:00', '2016-06-07 15:02:06', '2017-05-29 02:44:00', 0, 'admin', 'm', 'admin m', 'zn1nL7jG03.jpg', '0', '', '1', '0', '', '0', 0, '0000-00-00', '0000-00-00', '0', '', '', '0.00'),
(65, 2, 'support@dotcomweavers.com', '$2y$10$deBRJezglTYHNZukXaWWBOCEi02lXGwL9OfLxv4yp3gxesJ18xDYW', NULL, NULL, '2017-05-31 02:49:41', '2017-06-01 06:43:16', 0, 'Kiran ', 'Kumar', 'Kiran Kumar', 'zn1nL7jG03.jpg', '0', '(123) 456-4512', '1', 'cus_AkwXkZXcsHB3PZ', '594f534b85a5e66879981993eac5f0a9', 'IoOrQ2WDVDw7PNR2uitCMh9P26OcG8EFeE3FwuRToBjSakkbwnSuC5npy6cB', 0, '0000-00-00', '0000-00-00', '0', '', '', '0.00'),
(66, 2, 'soniya@gmail.com', '$2y$10$6CAyysND5oKfOpXRTIYWHOVXVJ3NTWJgRbfBDTj29qrc4beTWtbqa', NULL, NULL, '2017-05-31 03:32:18', '2017-05-31 07:37:18', 0, 'soniya', 'Basireddy', 'soniya Basireddy', 'zn1nL7jG03.jpg', '0', '(123) 456-4512', '1', 'cus_AkxDe8heDlzcAR', '22066f5c7e4787e3f252d458bf56d49e', 'wUOWU8tcGkjqJBMh7BiEvF9aOltqlrF9JKVht3oOwo7TFmvCbmKZBAR8O8cM', 0, '0000-00-00', '0000-00-00', '0', '', '', '0.00'),
(86, 4, 'jithendertest1@dotcomweavers.com', '$2y$10$gJv4O5pBro7DLP2Vj7M0i.bI55ZFMsoQBWNCj3wB9i8ZtodpxyXu6', NULL, NULL, '2017-06-06 07:25:43', '2017-06-06 07:41:21', 0, 'jithender test1', 'kumar', 'jithender test1 kumar', NULL, '0', '(121) 212-1212', '1', '403465473', 'efa87f9f91878be48b1b425534e3f0f0', NULL, 0, '0000-00-00', '0000-00-00', '0', '', '', '0.00'),
(87, 4, 'jithendertest2@dotcomweavers.com', '$2y$10$4D2EXDI9fM/KzH492Sp5k./ia05fr9HU.DVPHl4YFTr.WiQaTp8Fa', NULL, NULL, '2017-06-06 07:26:13', '2017-06-06 09:21:59', 0, 'jithendertest2', 'kumar', 'jithendertest2 kumar', NULL, '0', '', '1', '677716764', 'a80fb6dbe8fc0401c0b7b689ad251b08', NULL, 0, '0000-00-00', '0000-00-00', '1', '', '', '0.00'),
(88, 4, 'jithender@dotcomweavers.com', '$2y$10$aSUS/58rgLevKPndHU2CX.2tNAhBKdyN4zj615E4mhWLMbqktwCiW', NULL, NULL, '2017-06-06 07:40:37', '2017-06-06 11:33:50', 0, 'jithender', 'kumar', 'jithender kumar', NULL, '0', '', '1', '289998569', '061f6644113315f36383a39d574158cb', 'QrbjPWRXcIi07az4z2ffoaRawOMRJ5pQ0bEwnjbiMVf6cuuXvWnEUEkToMra', 0, '0000-00-00', '0000-00-00', '0', '', '', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `cc_user_address`
--

CREATE TABLE `cc_user_address` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_user_credit_log`
--

CREATE TABLE `cc_user_credit_log` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `credit` float NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `request_id` int(11) NOT NULL DEFAULT '0',
  `notes` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_user_meta`
--

CREATE TABLE `cc_user_meta` (
  `meta_id` int(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_us_states`
--

CREATE TABLE `cc_us_states` (
  `id` int(11) NOT NULL,
  `state_name` varchar(32) NOT NULL,
  `state_abbr` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cc_address_master`
--
ALTER TABLE `cc_address_master`
  ADD PRIMARY KEY (`address_id`),
  ADD UNIQUE KEY `address_id` (`address_id`,`address_type`);

--
-- Indexes for table `cc_attributes`
--
ALTER TABLE `cc_attributes`
  ADD PRIMARY KEY (`attribute_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `cc_attribute_options`
--
ALTER TABLE `cc_attribute_options`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Indexes for table `cc_blog_categories`
--
ALTER TABLE `cc_blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_blog_posts`
--
ALTER TABLE `cc_blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_cart`
--
ALTER TABLE `cc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cookie_id` (`cookie_id`);

--
-- Indexes for table `cc_cart_items`
--
ALTER TABLE `cc_cart_items`
  ADD PRIMARY KEY (`cart_item_id`),
  ADD KEY `costume_id` (`costume_id`),
  ADD KEY `cart_id` (`cart_id`);

--
-- Indexes for table `cc_category`
--
ALTER TABLE `cc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `cc_category_description`
--
ALTER TABLE `cc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `cc_charities`
--
ALTER TABLE `cc_charities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suggested_by` (`suggested_by`);

--
-- Indexes for table `cc_cms_blocks`
--
ALTER TABLE `cc_cms_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_cms_pages`
--
ALTER TABLE `cc_cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_conversations`
--
ALTER TABLE `cc_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_costumes`
--
ALTER TABLE `cc_costumes`
  ADD PRIMARY KEY (`costume_id`);

--
-- Indexes for table `cc_costumes_like`
--
ALTER TABLE `cc_costumes_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_costume_attribute_options`
--
ALTER TABLE `cc_costume_attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `costume_id` (`costume_id`),
  ADD KEY `attribute_id` (`attribute_id`),
  ADD KEY `attribute_option_value_id` (`attribute_option_value_id`);

--
-- Indexes for table `cc_costume_description`
--
ALTER TABLE `cc_costume_description`
  ADD PRIMARY KEY (`costume_id`,`language_id`),
  ADD KEY `name` (`name`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `cc_costume_image`
--
ALTER TABLE `cc_costume_image`
  ADD PRIMARY KEY (`costume_image_id`),
  ADD KEY `costume_idss` (`costume_id`),
  ADD KEY `costume_id` (`costume_id`);

--
-- Indexes for table `cc_costume_to_category`
--
ALTER TABLE `cc_costume_to_category`
  ADD PRIMARY KEY (`costume_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `cc_countries`
--
ALTER TABLE `cc_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_coupon_category`
--
ALTER TABLE `cc_coupon_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupon_id` (`coupon_id`) USING BTREE;

--
-- Indexes for table `cc_coupon_costumes`
--
ALTER TABLE `cc_coupon_costumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `costume_id` (`costume_id`);

--
-- Indexes for table `cc_coupon_history`
--
ALTER TABLE `cc_coupon_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_creditcard`
--
ALTER TABLE `cc_creditcard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_customer_wishlist`
--
ALTER TABLE `cc_customer_wishlist`
  ADD PRIMARY KEY (`user_id`,`costume_id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cc_events`
--
ALTER TABLE `cc_events`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `address_id` (`address_id`),
  ADD KEY `mentioned_user_id` (`mentioned_user_id`);

--
-- Indexes for table `cc_event_tags`
--
ALTER TABLE `cc_event_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_language`
--
ALTER TABLE `cc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cc_menu`
--
ALTER TABLE `cc_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `cc_menu_description`
--
ALTER TABLE `cc_menu_description`
  ADD PRIMARY KEY (`menu_id`,`language_id`);

--
-- Indexes for table `cc_messages`
--
ALTER TABLE `cc_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `conversation_id` (`conversation_id`);

--
-- Indexes for table `cc_order`
--
ALTER TABLE `cc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `cc_order_charity`
--
ALTER TABLE `cc_order_charity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_order_disputes`
--
ALTER TABLE `cc_order_disputes`
  ADD PRIMARY KEY (`dispute_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cc_order_history`
--
ALTER TABLE `cc_order_history`
  ADD PRIMARY KEY (`order_history_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `order_status_id` (`order_status_id`);

--
-- Indexes for table `cc_order_items`
--
ALTER TABLE `cc_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `costume_id` (`costume_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cc_order_ship_track`
--
ALTER TABLE `cc_order_ship_track`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cc_order_status`
--
ALTER TABLE `cc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`status_id`);

--
-- Indexes for table `cc_order_total`
--
ALTER TABLE `cc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cc_permissions`
--
ALTER TABLE `cc_permissions`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `cc_press`
--
ALTER TABLE `cc_press`
  ADD PRIMARY KEY (`press_id`);

--
-- Indexes for table `cc_press_categories`
--
ALTER TABLE `cc_press_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_press_cat_link`
--
ALTER TABLE `cc_press_cat_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `press_id` (`press_id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `cc_promotion_coupon`
--
ALTER TABLE `cc_promotion_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `cc_reported_costumes`
--
ALTER TABLE `cc_reported_costumes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_request_bags`
--
ALTER TABLE `cc_request_bags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `addres_id` (`addres_id`);

--
-- Indexes for table `cc_request_credits`
--
ALTER TABLE `cc_request_credits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `cc_request_shippings`
--
ALTER TABLE `cc_request_shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `cc_request_status`
--
ALTER TABLE `cc_request_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `cc_roles`
--
ALTER TABLE `cc_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `cc_role_permissions`
--
ALTER TABLE `cc_role_permissions`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `cc_states`
--
ALTER TABLE `cc_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_status`
--
ALTER TABLE `cc_status`
  ADD PRIMARY KEY (`status_id`,`language_id`);

--
-- Indexes for table `cc_tickets`
--
ALTER TABLE `cc_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_transactions`
--
ALTER TABLE `cc_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_url_rewrites`
--
ALTER TABLE `cc_url_rewrites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cc_users`
--
ALTER TABLE `cc_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `cc_user_address`
--
ALTER TABLE `cc_user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `address_id` (`address_id`);

--
-- Indexes for table `cc_user_credit_log`
--
ALTER TABLE `cc_user_credit_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `cc_user_meta`
--
ALTER TABLE `cc_user_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cc_us_states`
--
ALTER TABLE `cc_us_states`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cc_address_master`
--
ALTER TABLE `cc_address_master`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;
--
-- AUTO_INCREMENT for table `cc_attributes`
--
ALTER TABLE `cc_attributes`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `cc_attribute_options`
--
ALTER TABLE `cc_attribute_options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `cc_blog_categories`
--
ALTER TABLE `cc_blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cc_blog_posts`
--
ALTER TABLE `cc_blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cc_cart`
--
ALTER TABLE `cc_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `cc_cart_items`
--
ALTER TABLE `cc_cart_items`
  MODIFY `cart_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=465;
--
-- AUTO_INCREMENT for table `cc_category`
--
ALTER TABLE `cc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `cc_charities`
--
ALTER TABLE `cc_charities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `cc_cms_blocks`
--
ALTER TABLE `cc_cms_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_cms_pages`
--
ALTER TABLE `cc_cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cc_conversations`
--
ALTER TABLE `cc_conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `cc_costumes`
--
ALTER TABLE `cc_costumes`
  MODIFY `costume_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `cc_costumes_like`
--
ALTER TABLE `cc_costumes_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_costume_attribute_options`
--
ALTER TABLE `cc_costume_attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1888;
--
-- AUTO_INCREMENT for table `cc_costume_image`
--
ALTER TABLE `cc_costume_image`
  MODIFY `costume_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;
--
-- AUTO_INCREMENT for table `cc_countries`
--
ALTER TABLE `cc_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `cc_coupon_category`
--
ALTER TABLE `cc_coupon_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_coupon_costumes`
--
ALTER TABLE `cc_coupon_costumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_coupon_history`
--
ALTER TABLE `cc_coupon_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_creditcard`
--
ALTER TABLE `cc_creditcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cc_customer_wishlist`
--
ALTER TABLE `cc_customer_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_events`
--
ALTER TABLE `cc_events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cc_event_tags`
--
ALTER TABLE `cc_event_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_language`
--
ALTER TABLE `cc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cc_menu`
--
ALTER TABLE `cc_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `cc_messages`
--
ALTER TABLE `cc_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `cc_order`
--
ALTER TABLE `cc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `cc_order_charity`
--
ALTER TABLE `cc_order_charity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cc_order_history`
--
ALTER TABLE `cc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_order_items`
--
ALTER TABLE `cc_order_items`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `cc_order_ship_track`
--
ALTER TABLE `cc_order_ship_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cc_order_status`
--
ALTER TABLE `cc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `cc_order_total`
--
ALTER TABLE `cc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `cc_permissions`
--
ALTER TABLE `cc_permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cc_press`
--
ALTER TABLE `cc_press`
  MODIFY `press_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `cc_press_categories`
--
ALTER TABLE `cc_press_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cc_press_cat_link`
--
ALTER TABLE `cc_press_cat_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `cc_promotion_coupon`
--
ALTER TABLE `cc_promotion_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cc_reported_costumes`
--
ALTER TABLE `cc_reported_costumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_request_bags`
--
ALTER TABLE `cc_request_bags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_request_credits`
--
ALTER TABLE `cc_request_credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_request_shippings`
--
ALTER TABLE `cc_request_shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_request_status`
--
ALTER TABLE `cc_request_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_roles`
--
ALTER TABLE `cc_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_states`
--
ALTER TABLE `cc_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `cc_status`
--
ALTER TABLE `cc_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cc_tickets`
--
ALTER TABLE `cc_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `cc_transactions`
--
ALTER TABLE `cc_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `cc_url_rewrites`
--
ALTER TABLE `cc_url_rewrites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `cc_users`
--
ALTER TABLE `cc_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `cc_user_address`
--
ALTER TABLE `cc_user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_user_credit_log`
--
ALTER TABLE `cc_user_credit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_user_meta`
--
ALTER TABLE `cc_user_meta`
  MODIFY `meta_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cc_us_states`
--
ALTER TABLE `cc_us_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cc_cart_items`
--
ALTER TABLE `cc_cart_items`
  ADD CONSTRAINT `cc_cart_items_ibfk_2` FOREIGN KEY (`cart_id`) REFERENCES `cc_cart` (`cart_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_category_description`
--
ALTER TABLE `cc_category_description`
  ADD CONSTRAINT `cc_category_description_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `cc_category` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_category_description_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `cc_language` (`language_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_charities`
--
ALTER TABLE `cc_charities`
  ADD CONSTRAINT `cc_charities_ibfk_1` FOREIGN KEY (`suggested_by`) REFERENCES `cc_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cc_costume_description`
--
ALTER TABLE `cc_costume_description`
  ADD CONSTRAINT `cc_costume_description_ibfk_1` FOREIGN KEY (`costume_id`) REFERENCES `cc_costumes` (`costume_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_costume_description_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `cc_language` (`language_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_costume_image`
--
ALTER TABLE `cc_costume_image`
  ADD CONSTRAINT `cc_costume_image_ibfk_1` FOREIGN KEY (`costume_id`) REFERENCES `cc_costumes` (`costume_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_costume_to_category`
--
ALTER TABLE `cc_costume_to_category`
  ADD CONSTRAINT `cc_costume_to_category_ibfk_1` FOREIGN KEY (`costume_id`) REFERENCES `cc_costumes` (`costume_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_costume_to_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `cc_category` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_coupon_category`
--
ALTER TABLE `cc_coupon_category`
  ADD CONSTRAINT `cc_coupon_category_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `cc_promotion_coupon` (`coupon_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_menu_description`
--
ALTER TABLE `cc_menu_description`
  ADD CONSTRAINT `cc_menu_description_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `cc_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_order_disputes`
--
ALTER TABLE `cc_order_disputes`
  ADD CONSTRAINT `cc_order_disputes_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `cc_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_order_disputes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `cc_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_order_items`
--
ALTER TABLE `cc_order_items`
  ADD CONSTRAINT `cc_order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `cc_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_order_ship_track`
--
ALTER TABLE `cc_order_ship_track`
  ADD CONSTRAINT `cc_order_ship_track_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `cc_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_order_total`
--
ALTER TABLE `cc_order_total`
  ADD CONSTRAINT `cc_order_total_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `cc_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_request_bags`
--
ALTER TABLE `cc_request_bags`
  ADD CONSTRAINT `cc_request_bags_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `cc_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_request_bags_ibfk_2` FOREIGN KEY (`addres_id`) REFERENCES `cc_address_master` (`address_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_request_shippings`
--
ALTER TABLE `cc_request_shippings`
  ADD CONSTRAINT `cc_request_shippings_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `cc_request_bags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cc_role_permissions`
--
ALTER TABLE `cc_role_permissions`
  ADD CONSTRAINT `cc_role_permissions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `cc_roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cc_role_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `cc_permissions` (`permission_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
