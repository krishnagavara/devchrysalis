<?php $__env->startSection('title'); ?> @parent

<?php $__env->stopSection(); ?>


<?php $__env->startSection('header_styles'); ?>

    <link rel="stylesheet" href="<?php echo e(asset('/vendors/sweetalert/dist/sweetalert.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/admin/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/frontend/css/pages/drop_uploader.css')); ?>">
    <script src="<?php echo e(asset('/assets/admin/js/fileinput.js')); ?>"></script>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>Content Management System</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo e(url('/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="<?php echo e(url('/cms-blocks')); ?>">CMS Blocks</a>
            </li>

            <li class="active">Add Block</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title col-md-12 heading-agent">Add Block</h3>
                    </div>

                    <div class="box-body">
                        <?php if(Session::has('error')): ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo e(Session::get('error')); ?>

                            </div>
                        <?php elseif(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo e(Session::get('success')); ?>

                            </div>
                        <?php endif; ?>

                        <form id="add_cms_block" class="form-horizontal defult-form" name="addCmsBlock" action="<?php echo e(route('store-cms-block')); ?>" method="POST" autocomplete="off" enctype="multipart/form-data">

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback" >
                                        <label for="block_title" class="control-label">Block Title<span class="req-field" >*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter Block Title"  name="title" id="block_title">
                                        <p class="error"><?php echo e($errors->first('title')); ?></p>
                                        <span id="block_title_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group has-feedback" >
                                        <label for="pages" class="control-label">Pages<span class="req-field" >*</span></label>
                                        <select class="form-control" name="slug" id="pages">
                                            <?php $__currentLoopData = $pagesData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slug => $pageName): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <option value="<?php echo e($slug); ?>"><?php echo e($pageName); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </select>
                                        <p class="error"><?php echo e($errors->first('slug')); ?></p>
                                        <span id="page_title_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="description" class="control-label">Page Description<span class="req-field" >*</span></label>
                                        <textarea id="description" class="form-control" name="description"></textarea>
                                        <p class="error"><?php echo e($errors->first('description')); ?></p>
                                        <span id="page_desc_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="/cms-blocks" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</a>
                            <button type="submit" class="btn btn-info pull-right save-page">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('/js/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendors/sweetalert/dist/sweetalert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/assets/admin/js/pages/cms.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('/assets/frontend/vendors/drop_uploader/drop_uploader.js')); ?>"></script>
    <script src="<?php echo e(asset('ckeditor/ckeditor/ckeditor.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            CKEDITOR.replace( 'description' );
        });
    </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>