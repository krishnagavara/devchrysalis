<?php $__env->startSection('styles'); ?>
<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
<link rel="stylesheet" href="<?php echo e(asset('assets/frontend/css/pages/drop_uploader.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/frontend/css/pages/costumes_list.css')); ?>">
<style>
.charity_rigt .ct3-list li p {
    display: block;
}
.ct3-list li{min-height: 190px;}
.ct3-list li img{width:85px;}
h2.prog-head span {
    font-size: 14px;
    margin-left: 5px;
    font-family: Proxima-Nova-regular;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section class="content create_section_page">



<div class="container">
<div class="row">
<div class="col-md-12">

<!--- progressbar section starts -->
<div class="progressbar_main " >
<h2>UPLOAD A COSTUME</h2>
<ul id="progressbar" class="progressbar_rm hidden-xs">  
<li class="active" id="step1"><span class="s-head">Step 1</span> <span>Upload <br/>Photos</span></li> 
<li id="step2"><span class="s-head">Step 2</span> <span>Fill in Costume <br/>Description</span></li>
<li id="step3"><span class="s-head">Step 3</span> <span>Pricing & <br/>Shipping</span></li>
<li id="step4"><span class="s-head">Step 4</span> <span>Review <br/>Preferences</span></li>	 
</ul>
</div>	

<!---mobile progressbar section starts -->
<?php //echo $costume_details->donation_amount;die; ?>
<div class="progressbar_main hidden-sm hidden-md hidden-lg" style="display:none;">
<h2>UPLOAD A COSTUME</h2>
<ul id="progressbar" class="progressbar_rm" style="display:none;">  
<li class="active" id="step1"><span class="s-head">Step 1</span> <span>Upload <br/>Photos</span></li> 
<li id="step2"><span class="s-head">Step 2</span> <span>Fill in Costume <br/>Description</span></li>
<li id="step3"><span class="s-head">Step 3</span> <span>Pricing & <br/>Shipping</span></li>
<li id="step4"><span class="s-head">Step 4</span> <span>Review <br/>Preferences</span></li>	 
</ul>
</div>	
<!--- mobile progressbar section end here -->

<div id="total_forms_div">
<form enctype="multipart/form-data" role="form" class="validation" novalidate="novalidate"  name="costume_total_form" id="costume_total_form" method="post">
<!--Create costume image code starts here-->
<input type="hidden" name="costume_id" value="<?php echo e($costume_id); ?>">
<div class="upload-photo-blogs" id="upload_div">
<p class="prog-txt desk-pro-text">Please upload <span>the minimum required photos</span> of your costume in front, back and side view. Listings with more photos sell faster! Don't forget to include any accessories!</p>
<h2 class="prog-stepss  hidden-md hidden-lg hidden-sm">STEP 1</h2>
<h2 class="prog-head ">Upload Photos<span>(The optimal dimensions for the image is 260 x 356 pixels.)</span></h2>

<!--- mobile heaindgs section end here -->

<p class="prog-txt mobile-pro-text">Please upload <span>the minimum required photos</span> of your costume in front, back and side view. Listings with more photos sell faster! Don't forget to include any accessories!</p>

<!--- mobile heaindgs section end here -->
<div class="threeblogs">
<div class="col-md-3 col-sm-3 col-xs-12 upload_hint r">
<p><span class="up_tip">Tip</span> Respect your costume’s  integrity with crisp, clear photos. Placing them in settings that correspond with their theme can encourage a sale.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-12  c_pic " id="front_view">
<h4>01. Front View</h4>
<span class="remove_pic" id="drag_n_drop_1" >
<i class="fa fa-times-circle" aria-hidden="true"></i>				
</span>
<div class=" up-blog">
<!-- <img id="front_image_id" name="file1" src="<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$front_image->image; ?>"> -->
<input type="file" name="file1" accept="image/*" value="1" id="file1">
<?php if(isset($front_image->image) && !empty($front_image->image)){
	?>
	<div class="drop_uploader drop_zone"><ul class="files thumb">
		<li id="selected_file_0">
			<div class="thumbnail" style="background-image: url(<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$front_image->image; ?>)"></div></li></ul></div>
			<input type="hidden" name="hidden_file1" value="<?php echo e($front_image->image); ?>">
	 <?php 
}else { ?>
<input type="file" name="file1" accept="image/*" value="1" id="file1">
<?php 
} ?>

</div>
<span id="file1_error" style="color:red"></span>

</div>
<div class="col-md-3 col-sm-3 col-xs-12 rc_pic" id="back_view">
<h4>02. Back View</h4>
<span class="remove_pic" id="drag_n_drop_2" >
<i class="fa fa-times-circle" aria-hidden="true"></i>				
</span>
<div class=" up-blog">

<input type="file" name="file2" accept="image/*" id="file2" value="1">
<?php if(isset($back_image->image) && !empty($back_image->image)){
	?>
	<div class="drop_uploader drop_zone"><ul class="files thumb"><li id="selected_file_1"><div class="thumbnail" style="background-image: url(<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$back_image->image; ?>)"></div></li></ul></div>
			<input type="hidden" name="hidden_file2" value="<?php echo e($back_image->image); ?>">

	 <?php 
}else { ?>
<input type="file" name="file2" accept="image/*" id="file2" value="1">
<?php 
} ?>
</div>
<span id="file2_error" style="color:red"></span>

</div>
<div class="col-md-3 col-sm-3 col-xs-12 rc_pic " id="details_view">
<h4>03. Additional</h4>
<span class="remove_pic" id="drag_n_drop_3" <?php if(empty($details_image->image)): ?> style="display: none;" <?php endif; ?>>
<i class="fa fa-times-circle" aria-hidden="true"></i>					
</span>
<div class=" up-blog">

<input type="file" name="file3" accept="image/*" id="file3" value="1">
<?php if(isset($details_image->image) && !empty($details_image->image)){
	?>
	<div class="drop_uploader drop_zone"><ul class="files thumb"><li id="selected_file_2"><div class="thumbnail" style="background-image: url(<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$details_image->image; ?>)"></div></li></ul></div>
			<input type="hidden" name="hidden_file3" value="<?php echo e($details_image->image); ?>">

	 <?php 
}else { ?>
<input type="file" name="file3" accept="image/*" id="file3" value="1"><?php 
} ?>
</div>
<span id="file3_error" style="color:red"></span>

</div>

</div>
<div class=" up_btns_tl col-md-12 col-sm-12 col-xs-12">


<div class="col-md-12 col-sm-12 col-xs-12 ">
<div class="clearfix"></div>
<!-- <form> -->
<?php 
$i=0;
 ?>
<?php if(isset($more_image) && !empty($more_image)): ?>
<div class="row multi_main_div">

<?php $__currentLoopData = $more_image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<?php //echo "<pre>"; print_r($image->image);die; ?>
			<div class="col-md-4 col-sm-4 col-xs-12 multi_div" >
			<input type="hidden" name="hidden_file4[]" value="<?php echo e($image->image); ?>">

				<div class="multi_thumbs pip" style="background-image: url(<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$image->image; ?>)">
				<!-- <img class="imageThumb img-responsive" src="<?php echo e(asset('costumers_images/Medium')); ?><?php echo '/'.$image->image; ?>" title="undefined"><br><span class="remove"></span>-->
				<span class="remove_pic remove ">
				<i class="fa fa-times-circle" aria-hidden="true"></i>				
				</span>
				</div>
			</div> 
			
				
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<div id="other_thumbnails">
<!--<div class="col-md-3 col-sm-3 col-xs-12"></div>-->
</div>
</div>
<?php endif; ?>
</div>
<span id="fileselector">
<label class="btn btn-default upload_more_btn" for="upload-file-selector">
<input id="upload-file-selector" accept="image/*" type="file" name="file4[]" multiple>
<i class="fa_icon icon-upload-alt margin-correction"></i> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Upload More
</label>
</span>
<div class=" up_btns_tl col-md-12 col-sm-12 col-xs-12">
<a type="button" id="upload_next" class=" upload_sub_btn btn btn-default nxt">Next Step</a>
</div>
<!-- </form> -->
</div>
</div>

</div>

<!--- progressbar section End -->
<!--Second div code starts here-->

<!-- </div> -->
<div id="costume_description">
<?php //echo "<pre>";print_r($costume_description);die; ?>
<p class="prog-txt desk-pro-text">Please fill in the following fields  <span>as accurately as possible</span> to prevent disputes.</p>
<h2 class="prog-stepss  hidden-md hidden-lg hidden-sm">step 2</h2>
<h2 class="prog-head">Costume Description</h2>
<p class="prog-txt mobile-pro-text">Please fill in the following fields  <span>as accurately as possible</span> to prevent disputes.</p>
<!-- <form enctype="multipart/form-data" role="form" class="validation" novalidate="novalidate"  name="costume_description_form" id="costume_description_form" method="post"> -->	

<div class="prog-form-rm">
<div class="col-md-6">
<!--costume name code starts here-->
<div class="form-rms">
<p class="form-rms-que">01. Name Your Costume!*</p>
<p class="form-rms-input"><input type="text" name="costume_name" value="<?php echo e($costume_description->name); ?>" id="costume_name" autocomplete="off" tab-index="1" placeholder=""></p>
<span id="costumename_error" style="color:red"></span>
<p class="form-rms-small"><span>Give Your listing a descriptive title.</span> <br/>Example: "Men's Medium Spiderman<br/>Costume in Red"</p>
</div>
<!--costume name ends starts here-->
<!--Catgeory code starts here-->
<div class="form-rms">
<p class="form-rms-que">02. Category*</p>
<p class="form-rms-input">
<select name="categoryname" id="categoryname" >
<option value="">Select Category</option>
<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<option <?php if($costume_details->cat_id == $category->categoryid) { ?> selected="selected" <?php } ?> value="<?php echo e($category->categoryid); ?>"><?php echo e($category->categoryname); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>

</p>
<span id="categoryerror" style="color:red"></span>
</div>
<!--category code ends here-->
<!--Gender Code starts here-->
<div class="form-rms">
<p class="form-rms-que">03. Sex*</p>
<p class="form-rms-input">
<select name="gender" id="gender">
<option value="">Select Gender</option>
<option <?php if ($costume_details->gender == "male") { ?> selected="selected" <?php } ?> value="male">Male</option>
<option <?php if ($costume_details->gender == "female") { ?> selected="selected" <?php } ?> value="female">Female</option>
<option <?php if ($costume_details->gender == "unisex") { ?> selected="selected" <?php } ?> value="unisex">Unisex</option>
<option <?php if ($costume_details->gender == "pet") { ?> selected="selected" <?php } ?> value="pet">Pet</option>
</select>
</p>
<span id="gendererror" style="color:red"></span>
</div>
<!--Gender code ends here-->
<!--size code starts here-->
<div class="form-rms">
<p class="form-rms-que">04. Size*</p>
<p class="form-rms-input">
<select name="size" id="size">
<option value="">Select Size</option>
<option <?php if ($costume_details->size == "1sz") { ?> selected="selected" <?php } ?> value="1sz">1SZ</option>
<option <?php if ($costume_details->size == "xxs") { ?> selected="selected" <?php } ?> value="xxs">XXS</option>
<option <?php if ($costume_details->size == "xs") { ?> selected="selected" <?php } ?> value="xs">XS</option>
<option <?php if ($costume_details->size == "s") { ?> selected="selected" <?php } ?> value="s">S</option>
<option <?php if ($costume_details->size == "m") { ?> selected="selected" <?php } ?> value="m">M</option>
<option <?php if ($costume_details->size == "l") { ?> selected="selected" <?php } ?> value="l">L</option>
<option <?php if ($costume_details->size == "xl") { ?> selected="selected" <?php } ?> value="xl">XL</option>
<option <?php if ($costume_details->size == "xxl") { ?> selected="selected" <?php } ?> value="xxl">XXL</option>
<option <?php if ($costume_details->size == "std") { ?> selected="selected" <?php } ?> value="std">STD</option>
</select>
</p>
<span id="sizeerror" style="color:red"></span>
</div>
<!--size code ends here-->
<!--Get subcategory ajax code starts here-->
<div class="form-rms">
<p class="form-rms-que">05. Subcategory*</p>
<p class="form-rms-input">
	<?php //echo $db_subcategoryname;die; ?>
<?php if(count($db_subcategoryname)>0): ?>
<select name="subcategory" id="subcategory" >
<option value="">Select Sub Category</option>
<?php $__currentLoopData = $db_subcategoryname; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<option <?php if($costume_category_2->category_id == $category->subcategoryid) { ?> selected="selected" <?php } ?> value="<?php echo e($category->subcategoryid); ?>"><?php echo e($category->subcategoryname); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
<?php else: ?>
<select name="subcategory" id="subcategory">
<option value="">Select Sub Category</option>
</select>
<?php endif; ?>
</p>
<span id="subcategoryerror" style="color:red"></span>
</div>
<!--Get subcategory regarding categories code ends here-->


<div class="form-rms">
<p class="form-rms-que">06. Condition*</p>
<p class="form-rms-input">
<span class="full-rms"><input type="radio" name="condition" <?php if ($costume_details->condition == "brand_new") { ?> checked='checked' <?php } ?> value="brand_new" id="brandnew"> <label for="brandnew">Brand New</label></span>
<span class="full-rms"><input type="radio" name="condition" <?php if ($costume_details->condition == "like_new") { ?> checked='checked' <?php } ?> value="like_new" id="likenew"><label for="likenew"> Like New</label></span>
<span class="full-rms"><input type="radio" name="condition" <?php if ($costume_details->condition == "excellent") { ?> checked='checked' <?php } ?> value="excellent" id="excellent"> <label for="excellent">Excellent</label></span>
<span class="full-rms"><input type="radio" name="condition" <?php if ($costume_details->condition == "good") { ?> checked='checked' <?php } ?> value="good" id="good"> <label for="good">Good</label></span>
</p>
<span id="costumeconditionerror" style="color:red"></span>
</div>


<div class="form-rms">
<p class="form-rms-que">07. <?php echo e($bodyanddimensions->label); ?> (Optional)</p>
<div class="form-rms-input">
<?php
$value_height=$body_height_ft->value;
$explode_value_height=explode('-',$value_height);
$heading=$explode_value_height[0];
$heading_value=$explode_value_height[1];
$value_height_in=$body_height_in->value;
$explode_value_height_in=explode('-',$value_height_in);
$heading_value_in=$explode_value_height_in[1];
$value_weight=$body_weight_lbs->value;
$explode_value_weight=explode('-',$value_weight);
$heading_weight_value=$explode_value_weight[0];
$heading_weight_value_lbs=$explode_value_weight[1];
$value_chest=$body_chest_in->value;
$explode_value_chest=explode('-',$value_chest);
$heading_chest_value=$explode_value_chest[0];
$heading_chest_value_in=$explode_value_chest[1];
$value_waist=$body_waist_lbs->value;
$explode_value_waist=explode('-',$value_waist);
$heading_waist_value=$explode_value_waist[0];
$heading_waist_value_lbs=$explode_value_waist[1];
?>
<p class="form-rms-dim form-rms-he"><?php echo ucfirst($heading); ?> <br/> <span class="form-rms-he1">
<input type="<?php echo e($bodyanddimensions->code); ?>" value="<?php echo e($db_body_height_ft->attribute_option_value); ?>" name="<?php echo e($body_height_ft->value); ?>" id="<?php echo e($body_height_ft->value); ?>"> <span><?php echo $heading_value;?></span>
<input type="<?php echo e($bodyanddimensions->code); ?>" value="<?php echo e($db_body_height_in->attribute_option_value); ?>" class="form-rms-dt" name="<?php echo e($body_height_in->value); ?>" id="<?php echo e($body_height_in->value); ?>" > <span><?php echo $heading_value_in; ?></span></span></p>
<p class="form-rms-dim weight-chest"><?php echo ucfirst($heading_weight_value); ?> <br/> <span class="form-rms-he1"><input type="text" name="<?php echo e($body_weight_lbs->value); ?>" value="<?php echo e($db_body_weight_lbs->attribute_option_value); ?>" id="<?php echo e($body_weight_lbs->value); ?>"> <span><?php echo $heading_weight_value_lbs;?></span></span></p>
<p class="form-rms-dim weight-chest"><?php echo ucfirst($heading_chest_value); ?> <br/> <span class="form-rms-he1"><input type="text" name="<?php echo e($body_chest_in->value); ?>" value="<?php echo e($db_body_chest_in->attribute_option_value); ?>" id="<?php echo e($body_chest_in->value); ?>" > <span><?php echo $heading_chest_value_in; ?> </span></span></p>
<p class="form-rms-dim weight-chest"><?php echo ucfirst($heading_waist_value); ?> <br/> <span class="form-rms-he1"><input type="text" name="<?php echo e($body_waist_lbs->value); ?>" value="<?php echo e($db_body_waist_lbs->attribute_option_value); ?>" id="<?php echo e($body_waist_lbs->value); ?>"> <span><?php echo $heading_waist_value_lbs; ?></span></span></p>
<span id="bodydimensionerror"  style="color:red"></span>
</div>
</div>

<?php //echo count($db_cosplayplay_yes_opt);die; ?>
<div class="form-rms">
<p class="form-rms-que">08. <?php echo e($cosplayone->label); ?></p>
<p class="form-rms-input">
<?php $__currentLoopData = $cosplayone_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$cosplayone_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<span class="full-rms"><input type="<?php echo e($cosplayone->type); ?>" <?php if($db_cosplayone->attribute_option_value_id == $cosplayone_val->optionid) { ?> checked="checked" <?php } ?> name="<?php echo e($cosplayone->code); ?>" id="<?php echo e($cosplayone_val->optionid); ?>" value="<?php echo e($cosplayone_val->optionid); ?>"> <label for="<?php echo e($cosplayone_val->optionid); ?>"><?php echo e($cosplayone_val->value); ?></label></span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</p>
<span id="cosplayerror" style="color:red"></span>
<?php if(count($db_cosplayplay_yes_opt) == 1): ?>
<div class="row" id="cosplayplay_yes_div" <?php if(count($db_cosplayplay_yes_opt)!= 1): ?> style="display: none;" <?php endif; ?>>
<div class="col-md-12" >
<p class="slt_act_all">Select all that apply:</p>

<div class="fity_hlf">

<?php $__currentLoopData = $cosplaySubCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<div class="radio-inline">
		<label for="<?php echo e($subCat->name); ?>"><input type="radio" name="cosplayplay_yes_opt" value="<?php echo e($subCat->name); ?>" id="<?php echo e($subCat->name); ?>" <?php if($db_cosplayplay_yes_opt->attribute_option_value == $subCat->name): ?> checked="checked" <?php endif; ?>><?php echo e($subCat->name); ?></label>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

</div>



</div>

<span id="cosplay_yeserror" style="color:red"></span>
</div>
<?php endif; ?>
</div>

<div class="form-rms">
<p class="form-rms-que">09. <?php echo e($cosplaytwo->label); ?></p>
<p class="form-rms-input">
<?php $__currentLoopData = $cosplaytwo_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$cosplaytwo_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<span class="full-rms"><input type="<?php echo e($cosplaytwo->type); ?>" name="<?php echo e($cosplaytwo->code); ?>" <?php if($db_cosplaytwo->attribute_option_value_id == $cosplaytwo_val->optionid) { ?> checked="checked" <?php } ?> id="<?php echo e($cosplaytwo_val->optionid); ?>" value="<?php echo e($cosplaytwo_val->optionid); ?>"> <label for="<?php echo e($cosplaytwo_val->optionid); ?>"><?php echo e($cosplaytwo_val->value); ?></label></span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</p>
<span id="uniquefashionerror" style="color:red"></span>
<?php if(count($db_uniquefashion_yes_opt) == 1): ?>
<div class="row" id="uniquefashion_yes_div" <?php if(count($db_uniquefashion_yes_opt)!= 1): ?> style="display: none;" <?php endif; ?>>

<div class="col-md-12" >
<p class="slt_act_all">Select all that apply:</p>
<div class="fity_hlf">

<?php $__currentLoopData = $uniqueFashionSubCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<div class="radio-inline">
		<label for="<?php echo e($subCat->name); ?>"><input type="radio" name="uniquefashion_yes_opt" value="<?php echo e($subCat->name); ?>" id="<?php echo e($subCat->name); ?>" <?php if($db_uniquefashion_yes_opt->attribute_option_value == $subCat->name): ?> checked="checked" <?php endif; ?>><?php echo e($subCat->name); ?></label>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

</div>
</div>

<span id="uniquefashion_yeserror" style="color:red"></span>
</div>
<?php endif; ?>
</div>

</div>

<div class="col-md-6">

<div class="form-rms">
<p class="form-rms-que">10. <?php echo e($cosplaythree->label); ?></p>
<p class="form-rms-input">
<?php $__currentLoopData = $cosplaythree_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$cosplaythree_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<span class="full-rms"><input type="<?php echo e($cosplaythree->type); ?>" name="<?php echo e($cosplaythree->code); ?>" <?php if($db_cosplaythree->attribute_option_value_id == $cosplaythree_val->optionid) { ?> checked="checked" <?php } ?> id="<?php echo e($cosplaythree_val->optionid); ?>" value="<?php echo e($cosplaythree_val->optionid); ?>"> <label for="<?php echo e($cosplaythree_val->optionid); ?>"><?php echo e($cosplaythree_val->value); ?></label></span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</p>
<span id="activityerror" style="color:red"></span>
<?php if(count($db_activity_yes_opt) == 1): ?>
<div class="row" id="activity_yes_div" <?php if(count($db_activity_yes_opt) != 1): ?> style="display: none;" <?php endif; ?>>

<div class="col-md-12">
<p class="slt_act_all">Select all that apply:</p>

<div class="fity_hlf">

<?php $__currentLoopData = $filmTheatreSubCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<div class="radio-inline">
		<label for="<?php echo e($subCat->name); ?>"><input type="radio" name="activity_yes_opt" value="<?php echo e($subCat->name); ?>" id="<?php echo e($subCat->name); ?>" <?php if($db_activity_yes_opt->attribute_option_value == $subCat->name): ?> checked="checked" <?php endif; ?>><?php echo e($subCat->name); ?></label>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

</div>

</div>


<span id="activity_yeserror" style="color:red"></span>
</div>
<?php endif; ?>
</div>

<div class="form-rms">
<p class="form-rms-que">11. <?php echo e($cosplayfour->label); ?></p>
<p class="form-rms-input">
<?php $__currentLoopData = $cosplayfour_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$cosplayfour_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<span class="full-rms"><input type="<?php echo e($cosplayfour->type); ?>" name="<?php echo e($cosplayfour->code); ?>" <?php if($db_cosplayfour->attribute_option_value_id == $cosplayfour_val->optionid) { ?> checked="checked" <?php } ?> id="<?php echo e($cosplayfour_val->optionid); ?>" value="<?php echo e($cosplayfour_val->optionid); ?>"> <label for="<?php echo e($cosplayfour_val->optionid); ?>"> <?php echo e($cosplayfour_val->value); ?></label></span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php if(count($db_make_costume_time)== 1): ?>
<p class="form-rms-small" id="mention_hours" <?php if(count($db_make_costume_time)!= 1): ?> style="display: none;" <?php endif; ?> >If yes, how long did it take?</p>
<p class="ct1-rms-rel" id="mention_hours_input" <?php if(count($db_make_costume_time)!= 1): ?> style="display: none;" <?php endif; ?>><input type="text" name="make_costume_time" id="make_costume_time1" value="<?php echo e($db_make_costume_time->attribute_option_value); ?>" class="input-rm100"> <span>hours<span>
</p>
<?php else: ?>
<p class="form-rms-small" id="mention_hours" style="display: none;" >If yes, how long did it take?</p>
<p class="ct1-rms-rel  form-rms-input" id="mention_hours_input"style="display: none;">
	<input type="text" name="make_costume_time" id="make_costume_time1" class="input-rm100"> <span>hours<span>
</p>
<?php endif; ?>
<span id="usercostumeerror" style="color:red"></span>

</div>
<div class="form-rms">
<p class="form-rms-que">12. <?php echo e($cosplayfive->label); ?>*</p>
<p class="form-rms-input">
<?php $__currentLoopData = $cosplayfive_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$cosplayfive_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<span class="full-rms"><input type="<?php echo e($cosplayfive->type); ?>" name="<?php echo e($cosplayfive->code); ?>" id="<?php echo e($cosplayfive_val->optionid); ?>" <?php if($db_cosplayfive->attribute_option_value_id == $cosplayfive_val->optionid) { ?> checked="checked" <?php } ?> value="<?php echo e($cosplayfive_val->optionid); ?>"> <label for="<?php echo e($cosplayfive_val->optionid); ?>"><?php echo e($cosplayfive_val->value); ?></label></span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</p>
<?php if(count($db_film_name)== 1): ?>
<p class="form-rms-small" id="film_text" <?php if(count($db_film_name)!= 1): ?> style="display: none;" <?php endif; ?> >Which production was your costume featured in?</p>
<p class="ct1-rms-rel form-rms-input" id="film_text_input" <?php if(count($db_film_name)!= 1): ?> style="display: none;" <?php endif; ?>>
  <input type="text" name="film_name" value="<?php echo e($db_film_name->attribute_option_value); ?>" id="film_name" > <span><span>
</p>
<?php else: ?>
<p class="form-rms-small" id="film_text" style="display: none;">Which production was your costume featured in?</p>
<p class="ct1-rms-rel form-rms-input" id="film_text_input" style="display: none;">
  <input type="text" name="film_name" id="film_name" > <span><span>
</p>
<?php endif; ?>
<span id="qualityerror" style="color:red"></span>
</div>

<div class="form-rms descibr_smte_text">
<p class="form-rms-que form-rms-que1"><span>13. </span>How would you describe your costume?</p>
<p>Have a unique costume? Please enter a maximum of <strong>10</strong> keywords to describe it.</p>
<p><span class="ctume_tip-spn">Tip:</span>Have a speciailty costume? To increase your changes of making a sale, input the approprite keywords with our existing <span>list of categories.</span> </p>
<p class="form-rms-input"><input type="text" id="keywords_tag">
<a href="javascript:void(0)" id="keywords_add">Add</a>
</p>
<div id="div" class="keywords_div">
	<?php if(!empty($costume_description->keywords)): ?>
	<?php $explode = explode(',', $costume_description->keywords);
	 $keyword_count = count($explode);
	 
	foreach ($explode as $key => $keywords) {
		?>
		<?php if(!empty($keywords)): ?>
		<p class="keywords_p p_<?php echo e(10-$key); ?>"><?php echo e($keywords); ?><span id="remove_<?php echo e(10-$key); ?>">X</span> </p>

		<input id="input_<?php echo e(10-$key); ?>" name="keyword_<?php echo e(10-$key); ?>" value="<?php echo e($keywords); ?>" type="hidden">
		<?php endif; ?>
		<?php
	}
	
	for ($x = 10-$keyword_count+1; $x <= 10; $x++) {?>
	<input id="input_<?php echo e($x); ?>" name="keyword_<?php echo e($x); ?>" value="" type="hidden">
    
	<?php } ?>
	<?php endif; ?>
</div>
<div id="count"><?php if(!empty($costume_description->keywords)): ?><?php echo e(10 - count($explode)); ?> <?php endif; ?> left</div>

</div>

<div class="form-rms">
<p class="form-rms-que form-rms-que1"><span>14. Describe your Costume:</span> Including accessories*</p>
<p class="form-rms-input"><textarea placeholder="Please be as detailed as possible!" name="description" id="description" maxlength="600" ><?php echo e($db_des_costume->attribute_option_value); ?></textarea></p>

<span id="descriptionerror" style="color:red"></span>
<p class="form-rms-sm1">( <span id="max_length_char1"></span> 600 characters)</p>
</div>

<div class="form-rms">
<p class="form-rms-que form-rms-que1"><span>15. Fun Fact:</span> A little backstory to your costume and the adventures it has experienced</p>
<p class="form-rms-input"><textarea placeholder="Please be as detailed as possible!" name="funfcats" id="funfacts" maxlength="600" ><?php echo e($db_funfact->attribute_option_value); ?></textarea></p>
<span id="facterror" style="color:red"></span>
<p class="form-rms-sm1">( <span id="max_length_char2"></span> 600 characters)</p>
</div>

<div class="form-rms">
<p class="form-rms-que form-rms-que1"><span>16. FAQ </span>Create your own costume Frequently Asked Questions to avoid an overload of questions in your inbox!</p>
<p class="form-rms-input"><textarea placeholder="Please be as detailed as possible!" name="faq" id="faq" maxlength="600" ><?php echo e($db_faq->attribute_option_value); ?></textarea></p>
<span id="faqerror" style="color:red"></span>
<p class="form-rms-sm1">( <span id="max_length_char3"></span> 600 characters)</p>
<div class="form-rms-btn">
<a type="button" id="costume_description_back" class="btn-rm-back"><span>Back</span></a>

<!-- </form> -->
<a type="button" id="costume_description_next" class="btn-rm-nxt nxt">Next Step</a>
</div>
</div>

<!--costume three code starts here-->



<!--costume three code ends here-->
</div>
</div>

</div>
<div class="prog-form-rm" id="pricing_div">

<!-- <form enctype="multipart/form-data" role="form" class="validation" novalidate="novalidate"  name="costume_pricing_form" id="costume_pricing_form" method="post"> -->
<p class="prog-txt hidden-xs  ">Please fill in the following field <span>as accurately</span> as you can.</p>
<div class="row">
<div class="col-md-6">
<h2 class="prog-stepss  hidden-md hidden-lg hidden-sm">STEP 3</h2>
<h2 class="prog-head">Pricing</h2>

<p class="prog-txt hidden-md hidden-lg hidden-sm ">Please fill in the following field <span>as accurately</span> as you can.</p>
<div class="form-rms pricess pric_tag_three">
<p class="form-rms-que">01. Price</p>
<div class="form-rms-input">
<p class="form-rms-rel "><input type="text" class="input-rm100" value="<?php echo e(number_format($costume_details->price, 2)); ?>" name="price" id="price" ><span class="form-rms-abs"><i class="fa fa-usd" aria-hidden="true"></i></span></p>
<p class="cst2-textl2">Not Sure? <i class="fa fa-info-circle" aria-hidden="true"></i></p></div>
<span id="priceerror" style="color:red"></span>
</div>

<div class="form-rms">
<p class="form-rms-que">02. Quantity</p>
<p class="form-rms-input"><select  name="quantity" id="quantity" class="cst2-select50">
<option <?php if ($costume_details->quantity == '1') { ?> selected='selected' <?php } ?> >1</option>
<option <?php if ($costume_details->quantity == '2') { ?> selected='selected' <?php } ?> >2</option>
<option <?php if ($costume_details->quantity == '3') { ?> selected='selected' <?php } ?> >3</option>
<option <?php if ($costume_details->quantity == '4') { ?> selected='selected' <?php } ?> >4</option>
<option <?php if ($costume_details->quantity == '5') { ?> selected='selected' <?php } ?> >5</option>
<option <?php if ($costume_details->quantity == '6') { ?> selected='selected' <?php } ?> >6</option>
<option <?php if ($costume_details->quantity == '7') { ?> selected='selected' <?php } ?> >7</option>
<option <?php if ($costume_details->quantity == '8') { ?> selected='selected' <?php } ?> >8</option>
<option <?php if ($costume_details->quantity == '9') { ?> selected='selected' <?php } ?> >9</option>
<option <?php if ($costume_details->quantity == '10') { ?> selected='selected' <?php } ?> >10</option>
</select></p>
</div>
<span id="quantityerror" style="color:red"></span>
</div>

<div class="col-md-6">
<h2 class="prog-head snd-hdng">Package Information</h2>
<div class="form-rms">
<p class="form-rms-que">01. Weight of Packaged Item</p>
<div class="form-rms-input dimensions-two dimensions-two-pk_info">
<p class="form-rms-dim"><span class="form-rms-he1"><input id="pounds" name="pounds" value="<?php echo e($costume_details->weight_pounds); ?>" type="text"> <span>lbs</span></span></p>
<span id="poundserror" style="color:red"></span>
<p class="form-rms-dim"><span class="form-rms-he1"><input id="ounces" name="ounces" value="<?php echo e($costume_details->weight_ounces); ?>" type="text"> <span>oz </span></span></p>
<span id="ounceserror" style="color:red"></span>
</div>
<p class="ct3-rms-text">Note: Weight is applicable for one costume ONLY.</p>

</div>

<div class="form-rms">
<p class="form-rms-que">02. Dimensions</p>
<div class="form-rms-input dimensions-two dimensions-two-pk_info">
	<p class="form-rms-dim">Length <br> <span class="form-rms-he1"><input id="Length" name="Length" value="<?php if(!empty($db_dimensions_length->attribute_option_value)): ?> <?php echo e($db_dimensions_length->attribute_option_value); ?> <?php endif; ?>" type="text"> <span>in x</span></span></p>
<p class="form-rms-dim">Width <br> <span class="form-rms-he1"><input id="Width" name="Width" value="<?php if(!empty($db_dimensions_width->attribute_option_value)): ?><?php echo e($db_dimensions_width->attribute_option_value); ?> <?php endif; ?>" type="text"> <span>in x</span></span></p>
<p class="form-rms-dim">Height <br> <span class="form-rms-he1"><input id="Height" name="Height" value="<?php if(!empty($db_dimensions_height->attribute_option_value)): ?><?php echo e($db_dimensions_height->attribute_option_value); ?><?php endif; ?>" type="text"> <span>in </span></span></p>
</div>
<span id="dimensionserror" style="color:red"></span>

</div>

</div>
</div>
<div class="form-rms-btn">
<a type="button" id="pricing_back" class="btn-rm-back"><span>Back</span></a>

<a type="button" id="pricing_next" class="btn-rm-nxt nxt">Next Step</a>
</div>
<!-- </form> -->
</div>
<div class="prog-form-rm" id="preferences_div">

<!-- <form enctype="multipart/form-data" role="form" class="validation" novalidate="novalidate"  name="costume_preferences_form" id="costume_preferences_form" method="post"> -->
<p class="prog-txt  hidden-xs">You're almost done! Just a few more questions.</p>
<h2 class="prog-stepss  hidden-md hidden-lg hidden-sm">step 3</h2>
<h2 class="prog-head">Review Your Preferences</h2>

<div class="col-md-6">
<p class="prog-txt hidden-md hidden-lg hidden-sm ">You're almost done! Just a few more questions.</p>

<div class="form-rms">
<p class="form-rms-que">01. Handling Time <i class="fa fa-info-circle fa-info-rm" aria-hidden="true"></i></p>
<p class="form-rms-input">
<select name="handlingtime" id="handlingtime">
<option value="">Select Handling Time</option>
<?php $__currentLoopData = $handlingtime; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$handlingtime): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<option <?php if($db_handlingtime->attribute_option_value_id == $handlingtime->optionid) { ?> selected="selected" <?php } ?> value="<?php echo e($handlingtime->optionid); ?>"><?php echo e($handlingtime->value); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
</p>
<span id="handlingtimeerror" style="color:red"></span>

</div>


<div class="form-rms">
<p class="form-rms-que">02. Return Policy</p>
<p class="form-rms-input">
<select name="returnpolicy" id="returnpolicy" >
<option value="">Select Return Policy</option>
<?php $__currentLoopData = $returnpolicy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$returnpolicy): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<option <?php if($db_return->attribute_option_value_id == $returnpolicy->optionid) { ?> selected="selected" <?php } ?> value="<?php echo e($returnpolicy->optionid); ?>"><?php echo e($returnpolicy->value); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
</p>
<span id="returnpolicyerror" style="color:red"></span>

</div>

</div>
<?php //echo $costume_details->dynamic_percent; die;?>
<div class="col-md-6 charity_rigt">
<div class="form-rms lst-stp">
<p class="form-rms-que form-rms-que1 dnt_br">03. Donate a Portion to Charity</p>
<p class="ct3-rms-text">Chrysalis Charges a 3% transaction fee on sale of every costume.However, if you donate 5% or more of your sale to a charity we will waive our transcation fee to match your contribution</p>
<p class="ct3-rms-text">By Choosing to donate, I agree and accept Chrysalis' <a style="border-bottom: 1px solid #ccc">Terms & Conditions</a>.</p>
<p class="ct3-rms-head">Donation Amount</p>
<div class="form-rms-input">
<p class="form-rms-rel1">
<select class="cst2-select80" id="donate_charity" name="donate_charity">
	<option value="">Donate Amount</option>
	<option <?php if($costume_details->dynamic_percent == "none"): ?> selected="selected" <?php endif; ?> value="none">None</option>
	<option <?php if($costume_details->dynamic_percent == "10"): ?> selected="selected" <?php endif; ?> value="10">10%</option>
	<option <?php if($costume_details->dynamic_percent == "20"): ?> selected="selected" <?php endif; ?> value="20">20%</option>
	<option <?php if($costume_details->dynamic_percent == "30"): ?> selected="selected" <?php endif; ?> value="30">30%</option>
        <option <?php if($costume_details->dynamic_percent == "1"): ?> selected="selected" <?php endif; ?> value="1">1%</option>
</select></p>
<p class="cst3-textl2" name="dynamic_percent_amount" id="dynamic_percent_amount"><i class="fa fa-usd" aria-hidden="true"></i><?php echo e(number_format($costume_details->donation_amount, 2)); ?></p>
<input type="hidden" name="hidden_donation_amount" id="hidden_donation_amount" value="<?php echo e($costume_details->donation_amount); ?>">
<span id="donate_charityerror" style="color:red"></span>
</div>
<p class="ct3-rms-head">Donate to</p>
<ul class="ct3-list">
	<?php //echo $costume_details->charity_id;die; ?>
<?php $__currentLoopData = $charities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$charity): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<li><img src="<?php if(isset($charity->image) && !empty($charity->image)): ?><?php echo e(URL::asset('/charities_images/')); ?>/<?php echo e($charity->image); ?> <?php else: ?> <?php echo e(URL::asset('/img/default.png')); ?> <?php endif; ?>" alt="<?php echo e($charity->name); ?>" />
<p><?php echo e($charity->name); ?></p>
<input type="radio" id="<?php echo e($charity->name); ?>" <?php if($costume_details->charity_id == $charity->id): ?> checked="checked" <?php endif; ?> value="<?php echo e($charity->id); ?>" name="charity_name" /></li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</ul>
<span id="charity_nameerror" style="color:red"></span>

<p class="cst2-rms-chck"><input type="checkbox" id="another_charity" name="another_charity" <?php if($costume_details->charity_id == 0 && $costume_details->dynamic_percent != "none"): ?> checked="checked" <?php endif; ?> > I would like to suggest another charity organization</p>
</div>

<div class="form-rms" id="other_organzation_check" <?php if($costume_details->charity_id != 0 || $costume_details->dynamic_percent == "none" ): ?>style="display: none;" <?php endif; ?>>
<p class="ct3-rms-head chartiy_spcy">Please Specify:</p>
<p class="form-rms-input org_nme"><input type="text" value="<?php if(isset($charities_details) && !empty($charities_details)): ?><?php echo e($charities_details->name); ?><?php endif; ?>" name="organzation_name" id="organzation_name" autocomplete="off" placeholder="Organization Name"  class="form-control"></p>
<span id="organzation_nameerror" style="color:red"></span>

</div>





<div class="form-rms-btn loader-align">
<img id='ajax_loader' src="<?php echo e(asset('img/ajax-loader.gif')); ?>" style="display :none;" >	

<a type="button" id="preferences_finished" class="btn-rm-nxt">I'm Finished!</a>
<a type="button" id="preferences_back" class="btn-rm-back"><span>Back</span></a>
</div>
</div>
<!-- </form> -->
</div>
</form>
</div><!-- id='total_forms_div' -->
<div id="success_page" style="display: none;">
<div class="col-md-12">
<div class="row">
<div class="success_page_final">

<img class="img-responsive" src="<?php echo e(URL::asset('assets/frontend/img/chrysalis-meme.png')); ?>">
<h2>Success!</h2>
<p>Thank You for listing your costume with Chrysalis!<br>
Your costume has successfully been uploaded.</p>
<a type="button" id="" href="<?php echo e(URL::to('/')); ?>" class="btn-rm-ret">Return Home</a><br>
<a type="button" id="" href="<?php echo e(URL::to('my/costumes')); ?>" class="btn-rm-view-finl"> <span>View My Listing!<span></a>
</div>
</div>
</div>
</div>
<!-- </div> -->
<!-- </div> -->	
</div>	</div>		</div>	
<!-- </form> -->
<!---Second div code ends here-->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<!--<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
<script type="text/javascript" src="<?php echo e(asset('/assets/frontend/vendors/drop_uploader/drop_uploader.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/assets/frontend/js/costumesedit.js')); ?>"></script>
<!--Getting subcategory list by oonchange-->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('/frontend/app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>