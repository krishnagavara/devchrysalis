<?php $__env->startSection('title'); ?> @parent

<?php $__env->stopSection(); ?>


<?php $__env->startSection('header_styles'); ?>

    <link rel="stylesheet" href="<?php echo e(asset('/vendors/sweetalert/dist/sweetalert.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/admin/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/frontend/css/pages/drop_uploader.css')); ?>">
    <script src="<?php echo e(asset('/assets/admin/js/fileinput.js')); ?>"></script>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>FAQs</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo e(url('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="<?php echo e(url('manage-faqs')); ?>">FAQs</a>
            </li>

            <li class="active"><?php echo e($faq->title); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title col-md-12 heading-agent">Edit FAQ</h3>
                    </div>

                    <div class="box-body">
                        <?php if(Session::has('error')): ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo e(Session::get('error')); ?>

                            </div>
                        <?php elseif(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo e(Session::get('success')); ?>

                            </div>
                        <?php endif; ?>

                        <form id="edit_faq" class="form-horizontal defult-form" name="editFaq" action="/update-faq/<?php echo e($faq->id); ?>" method="POST" autocomplete="off" enctype="multipart/form-data">

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" name="status" value="<?php echo e($faq->id); ?>">
                            <div class="col-md-12">
                                <!-- post information starts here -->
                                <div class="col-md-8">
                                    <div class="form-group has-feedback" >
                                        <label for="faq_title" class="control-label">FAQ Title<span class="req-field" >*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter FAQ Title" value="<?php echo e($faq->title); ?>" name="title" id="faq_title">
                                        <p class="error"><?php echo e($errors->first('title')); ?></p>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="faq-block" class="control-label">FAQ Block<span class="req-field" >*</span></label>
                                        <select class="form-control" id="faq-block" name="block">
                                            <option default value="how-it-works">How It Works</option>
                                            <option value="support-and-contact">Support And Contact</option>
                                            <option value="sell-a-costume">Sell A Costume</option>
                                        </select>
                                        <p class="error"><?php echo e($errors->first('block')); ?></p>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="faq_description" class="control-label">FAQ Description<span class="req-field" >*</span></label>
                                        <textarea id="faq_description" class="form-control" name="faq_description"><?php echo e($faq->description); ?></textarea>
                                        <p class="error"><?php echo e($errors->first('faq_description')); ?></p>
                                        <span id="page_desc_error" style="color:red"></span>
                                    </div>

                                    <div class="form-group has-feedback" >
                                        <label for="sorting" class="control-label">Sort No.<span class="req-field" >*</span></label>
                                        <input type="text" name="sort_no" class="form-control" value="<?php echo e($faq->sort_no); ?>" id="sorting"/>
                                        <p class="error"><?php echo e($errors->first('sort_no')); ?></p>
                                        <span id="page_desc_error" style="color:red"></span>
                                    </div>
                                </div>
                                <!-- post information ends here -->
                            </div>

                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="/manage-faqs" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</a>
                            <button type="submit" class="btn btn-info pull-right">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('/js/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendors/sweetalert/dist/sweetalert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/assets/admin/js/pages/faqs.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('/assets/frontend/vendors/drop_uploader/drop_uploader.js')); ?>"></script>
    <script src="<?php echo e(asset('ckeditor/ckeditor/ckeditor.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            CKEDITOR.replace( 'faq_description' );

            var block = '<?= $faq->block ?>';
            $('#faq-block').val(block);

            $("#sorting").keydown(function (e) {
                // ANllow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        });
    </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>