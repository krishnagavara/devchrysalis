<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		

        <?php echo Meta::tag('title'); ?>

        <?php echo Meta::tag('description'); ?>

		<link rel="icon" type="image/png" href="<?php echo e(asset('img/favicon.png')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('/vendors/bootstrap/dist/css/bootstrap.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('/assets/frontend/css/chrysalis.css')); ?>">
		<link href="<?php echo e(asset('/assets/frontend/vendors/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo e(asset('/assets/frontend/vendors/lobibox-master/css/lobibox.css')); ?>">
		
		<?php echo $__env->yieldContent('styles'); ?>
	</head>
	<body ng-app="app">
		<div class="main-container">
			<section class="main_header">
			<?php echo $__env->make('frontend.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<?php echo $__env->make('frontend.partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</section>
			<?php echo $__env->yieldContent('content'); ?>
			<?php echo $__env->make('frontend.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
		<div class="img-loading hide"><img src="/img/chackout.gif"/></div>
      		
		<script src="<?php echo e(asset('/js/jquery-2.2.4.js')); ?>"></script>
		<script src="<?php echo e(asset('/vendors/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
		<script src="<?php echo e(asset('/angular/lib/angular.js')); ?>"></script>
		<script src="<?php echo e(asset('/angular/lib/angular-datatables.min.js')); ?>"></script>
		<script src="<?php echo e(asset('/vendors/datatables/jquery.dataTables.min.js')); ?>"></script>
		<script src="<?php echo e(asset('angular/lib/angular-datatables.min.js')); ?>"></script>
		<script src="<?php echo e(asset('/angular/app.js')); ?>"></script>
		<script src="<?php echo e(asset('/js/jquery.validate.min.js')); ?>"></script>
		<script src="<?php echo e(asset('/assets/frontend/js/custom.js')); ?>"></script>
		<script src="<?php echo e(asset('/angular/directives/datepicker.js')); ?>"></script>
		<script src="<?php echo e(asset('/assets/frontend/vendors/lobibox-master/js/notifications.js')); ?>"></script>
		
		
		
		<?php echo $__env->yieldContent('footer_scripts'); ?>
	</body>
</html>